#############################################################################################
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * #
# *                                                                                       * #
# *                              LANCEMENT DU NEUROSHELL NFB                              * #
# *                                                                                       * #
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * #
#############################################################################################

# Importations
import sys, sqlite3, datetime
from datetime import date
from Neurofeedback_Web import Patient, Entraineur, Protocole, Programme, Entrainement, Rapport
from flask import Flask, render_template, request

# NeuroShell
print ("\n\t\t\t  NEUROSHELL NFB 1.0")
print ("\t\t\t    Meriem Chergui")
print ("\t\t\t       2019/3/6")



#############################################################################################
#                                                                                           #
#                                           FLASK                                           # 
#                                                                                           #
#############################################################################################


app = Flask(__name__)
app.debug = True

@app.route('/<path:path>')
def all_files(path):
    return app.send_static_file(path)



#############################################################################################
#                                                                                           #
#                                      BASE DE DONNÉES                                      # 
#                                                                                           #
#############################################################################################

# Création/connection à la base de données locale	
conn   = sqlite3.connect('NFB.db')
cursor = conn.cursor()


# Création de la table patient (Patients_Table) si elle n'existe pas déjà
cursor.execute('''CREATE TABLE IF NOT EXISTS Patients_Table(identifiant INTEGER NOT NULL PRIMARY KEY, nom TEXT NOT NULL, prenom TEXT NOT NULL, age INTEGER NOT NULL, genre TEXT NOT NULL, casque TEXT NOT NULL)''')


# Création de la table entraineur (Entraineurs_Table) si elle n'existe pas déjà
cursor.execute('''CREATE TABLE IF NOT EXISTS Entraineurs_Table(identifiant INTEGER NOT NULL PRIMARY KEY, nom TEXT NOT NULL, prenom TEXT NOT NULL)''')


# Création de la table protocole (Protocoles_Table) si elle n'existe pas déjà
cursor.execute('''CREATE TABLE IF NOT EXISTS Protocoles_Table(protocole_id INTEGER NOT NULL PRIMARY KEY, nom_protocole TEXT NOT NULL, aires_broadmann TEXT NOT NULL)''')


# Création de la table programme (Programmes_Table) si elle n'existe pas déjà
cursor.execute('''CREATE TABLE IF NOT EXISTS Programmes_Table(programme_id INTEGER NOT NULL PRIMARY KEY, identifiant INTEGER NOT NULL,protocole_id INTEGER NOT NULL, priorite_clinique INTEGER NOT NULL, nombre_seances INTEGER NOT NULL, nom TEXT NOT NULL,prenom TEXT NOT NULL,FOREIGN KEY (identifiant) REFERENCES Patients_Table (identifiant) ON DELETE CASCADE, FOREIGN KEY (protocole_id) REFERENCES Entraineurs_Table (identifiant) ON DELETE CASCADE)''')


# Création de la table entrainement (Entrainements_Table) si elle n'existe pas déjà
cursor.execute('''CREATE TABLE IF NOT EXISTS Entrainements_Table(entrainement_id INTEGER NOT NULL PRIMARY KEY, date TEXT NOT NULL, heure TEXT NOT NULL, patient_id INTEGER NOT NULL,entraineur_id INTEGER NOT NULL,nom TEXT NOT NULL,prenom TEXT NOT NULL,FOREIGN KEY (patient_id) REFERENCES Patients_Table (identifiant) ON DELETE CASCADE, FOREIGN KEY (entraineur_id) REFERENCES Protocoles_Table (protocole_id) ON DELETE CASCADE)''')


# Création de la table rapport (Rapports_Table) si elle n'existe pas déjà
cursor.execute('''CREATE TABLE IF NOT EXISTS Rapports_Table (rapport_id INTEGER NOT NULL PRIMARY KEY, patient_id INTEGER NOT NULL, entrainements_requis INTEGER NOT NULL, entrainements_prevus INTEGER NOT NULL,programmes_patient TEXT NOT NULL,entrainements_patient TEXT NOT NULL,nom TEXT NOT NULL,prenom TEXT NOT NULL,FOREIGN KEY (patient_id) REFERENCES Patients_Table (identifiant) ON DELETE CASCADE)''')

conn.close()


#############################################################################################
#                                                                                           #
#                                      LISTES D'OBJETS                                      # 
#                                                                                           #
#############################################################################################

liste_patients        = []
liste_entraineurs     = []
liste_protocoles      = []
liste_programmes      = []
liste_entrainements   = []
liste_rapports        = []

#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Raffraichir toutes les listes					(Sur appel)	   -* 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def rafraichirListes():
	rafraichirPatient       ()
	rafraichirEntraineur    ()
	rafraichirProtocole     ()
	rafraichirProgramme     ()
	rafraichirEntrainement  ()
	rafraichirRapport       ()


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Appeler des objets                                              (Sur appel)        -*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def objectiser_patient(identifiant):
	for patient in liste_patients:
		if patient.getIdentifiant() == identifiant :
			return patient

def objectiser_entraineur(identifiant):
	for entraineur in liste_entraineurs:
		if entraineur.getIdentifiant() == identifiant :
			return entraineur

def objectiser_protocole(protocole_id):
	for protocole in liste_protocoles:
		if protocole.getIdentifiantProtocole() == protocole_id:
			return protocole

def objectiser_programme(programme_id):
	for programme in liste_programmes:
		if programme.getIdentifiantProgramme() == programme_id:
			return programme

def  objectiser_entrainement(entrainement_id):
	for entrainement in liste_entrainements:
		if entrainement.getIdentifiantEntrainement() == entrainement_id:
			return entrainement

def objectiser_rapport(rapport_id):
	for rapport in liste_rapports:
		if rapport.getIdentifiantRapport() == rapport_id:
			return rapport


#############################################################################################
#                                                                                           #
#                                          ERREUR                                           # 
#                                                                                           #
#############################################################################################

#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Entrée invalide                                                (Sur Appel)         -*  
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def entree_invalide():
	print("\nEntrée invalide\n")


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#- 	Message d'erreur pour une entrée invalide                       (Sur appel)        -* 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def messageErreur(invalide):
	entree_invalide()
	message_erreur = []
	message_erreur.append(invalide)
	return message_erreur



#############################################################################################
#                                                                                           #
#                                       MENU PRINCIPAL                                     # 
#                                                                                           #
#############################################################################################

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MENU PRINCIPAL                                                  (Choix 0-0-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/menu_principal')
def menuPrincipalParDefaut():
	print ("\n\n================================================================================")
	print("MENU PRINCIPAL")
	print ("================================================================================\n")
	return render_template('000_menu_principal.html')

@app.route ('/menu_principal', methods=['POST'])
def menuPrincipalAide():
	print ("\n\n--------------------------------------------------------------------------------\nMENU PRINCIPAL")
	support_usager = 1
	return render_template('000_menu_principal.html', support_usager = support_usager)




#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@#=                                                                                       =#@
#@#=                                  FONCTIONS PATIENT                                    =#@
#@#=                                                                                       =#@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#===========================================================================================#
#=	Mise à jour des patients                                       (Sur Appel)         =#
#===========================================================================================#

def rafraichirPatient():
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute(''' SELECT * FROM Patients_Table ORDER BY nom ASC,prenom ASC''')
	lignesTablePatient = cursor.fetchall()
	conn.close()
	del liste_patients [:]
	for attribut in lignesTablePatient:
		identifiant             = attribut[0]
		nom                     = attribut[1]
		prenom                  = attribut[2]
		age                     = attribut[3]	
		genre                   = attribut[4]
		casque                  = attribut[5]
		patient                 = Patient(identifiant,nom,prenom,age,genre,casque)
		liste_patients.append(patient)


#===========================================================================================#
#= 	Vérifier si un patient existe                                                      =#
#===========================================================================================#

def testerExistencePatient(identifiant):
	existence = []
	for patient in liste_patients:
		if str(identifiant) == str(patient.getIdentifiant()):
			existence.append(patient.getIdentifiant())
	if len(existence)!=0:
		return True
	else:
		return False


#===========================================================================================#
#=	Formater un patient pour HTML                                  (Sur Appel)         =#
#===========================================================================================#

def formaterPatientHTML1():
	patients_liste_X = []
	for patient in liste_patients:
		patientx=(patient.getIdentifiant(),patient.getNom(),patient.getPrenom(),patient.getAgePatient(),patient.getGenrePatient(),patient.getCasquePatient())
		patients_liste_X.append(patientx)
	return patients_liste_X


def formaterPatientChronoHTML():
	patients_liste_X = []
	for patient in liste_patients:
		patientx=(patient.getIdentifiant(),patient.getNom(),patient.getPrenom(),patient.getAgePatient(),patient.getGenrePatient(),patient.getCasquePatient())
		patients_liste_X.append(patientx)
	patients_liste_X.sort()
	for item in patients_liste_X:
		print("Identifiant :\t",item[0],"\nNom :\t\t",item[1],"\nPrénom :\t",item[2],"\nÂge :\t\t",item[3],"\nGenre :\t\t",item[4],"\nCasque :\t",item[5],"\n\n")
	return patients_liste_X


def formaterPatientHTML2(patient_id):
	patients_liste_Y = []
	patient = objectiser_patient(patient_id)
	patienty=(patient.getIdentifiant(),patient.getNom(),patient.getPrenom(),patient.getAgePatient(),patient.getGenrePatient(),patient.getCasquePatient())
	patients_liste_Y.append(patienty)
	return patients_liste_Y



#===========================================================================================#
#= 	Terminal : Afficher un patient à l'aide de son identifiant     (Sur Appel)         =#
#===========================================================================================#

def afficherUnPatient(identifiant):
	for patient in liste_patients:
		if patient.getIdentifiant() == identifiant:
			print("\nIdentifiant :\t",patient.getIdentifiant(),"\nNom :\t\t",patient.getNom(),"\nPrénom :\t",patient.getPrenom(),"\nÂge :\t\t",patient.getAgePatient(),"\nGenre :\t\t",patient.getGenrePatient(),"\nCasque :\t",patient.getCasquePatient(),"\n\n")



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MENU PATIENT                                                    (Choix 1-0-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/menu_patient')
def menuPatientParDefaut():
	print ("\n\n================================================================================")
	print("MENU PATIENT")
	print ("================================================================================\n")
	return render_template('100_menu_patient.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AJOUTER UN PATIENT                                              (Choix 1-1-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/ajouter_patient')
def ajouterPatientParDefaut():
	print ("\n\n================================================================================")
	print("AJOUTER UN PATIENT")
	print ("================================================================================\n")
	patients_liste_X = formaterPatientHTML1()
	return render_template('110_ajouter_patient.html', patients = patients_liste_X)


@app.route ('/ajouter_patient', methods=['POST'])
def ajouterPatient():
	print ("\n\n--------------------------------------------------------------------------------\nAJOUTER UN PATIENT")
	try:
		identifiant=genererIDPatient()+1
		nom = request.form["nom"]
		prenom = request.form["prenom"]
		age = request.form["age"]
		age = int(age)
		genre = request.form["genre"]
		casque = request.form["casque"]
		if nom != "" and prenom != "" and genre != "" and casque != "":
			patient = Patient(identifiant,nom,prenom,age,genre,casque)
			ajout_Patient_BD(patient)
			patients_liste_Y=formaterPatientHTML2(patient.getIdentifiant())
			return render_template('110_ajouter_patient.html', patient = patients_liste_Y)
		else:
			entree_invalide()
			return ajouterPatientParDefaut()
	except ValueError:
		entree_invalide()
		return ajouterPatientParDefaut()		


#===========================================================================================#
#=	Générer un nouvel identifiant patient                          (Sur Appel)         =#
#===========================================================================================#

def genererIDPatient():
	rafraichirPatient()
	ID=[]
	for patient in liste_patients:
		ID.append(patient.getIdentifiant())
	try:
		return max(ID)
	except ValueError:
		return 0


#===========================================================================================#
#=	Enregistrer le patient dans la BD                              (Sur Appel)         =#
#===========================================================================================#

def ajout_Patient_BD(patient):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute('''INSERT INTO Patients_Table(identifiant,nom,prenom,age,genre,casque) VALUES(?,?,?,?,?,?)''',(patient.getIdentifiant(),patient.getNom(),patient.getPrenom(),patient.getAgePatient(),patient.getGenrePatient(),patient.getCasquePatient()))
	conn.commit()
	conn.close()
	print("\n"+ patient.__str__() + " ajouté\n")
	rafraichirPatient()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	SUPPRIMER UN PATIENT                                            (Choix 1-2-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/supprimer_patient')
def supprimerUnPatientParDefaut():
	print ("\n\n================================================================================")
	print("SUPPRIMER UN PATIENT")
	print ("================================================================================\n")
	patients_liste_X=formaterPatientHTML1()
	return render_template('120_supprimer_patient.html', patients=patients_liste_X)


@app.route ('/supprimer_patient', methods=['POST'])
def supprimerPatientBD():
	print ("\n\n--------------------------------------------------------------------------------\nSUPPRIMER UN PATIENT")
	try:
		identifiant = request.form["patient_id"]
		identifiant = int(identifiant)
		if identifiant != "" and testerExistencePatient(identifiant) == True:
			patients_liste_Y=formaterPatientHTML2(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' DELETE FROM Patients_Table WHERE identifiant  = (?)''',(identifiant,))
			conn.commit()
			supprimerObjetPatient(identifiant)
			rafraichirPatient()
			return render_template('120_supprimer_patient.html', patient=patients_liste_Y)
		else:
			entree_invalide
			return supprimerUnPatientParDefaut()
		conn.close()
		rafraichirPatient()
	except ValueError:
		entree_invalide()
		return supprimerUnPatientParDefaut()



#===========================================================================================#
#=	Supprimer un patient de la BD                                  (Sur Appel)         =#
#===========================================================================================#

def supprimerObjetPatient(identifiant):
	for patient in liste_patients:
		if patient.getIdentifiant() == identifiant:
			print("\n\n"+ patient.__str__() + " supprimé\n")
			print("Effacement des données\n\n")
			verifierSiRapport(patient)
			del patient		
	rafraichirListes()


#===========================================================================================#
#=  	Vérifier et supprimer en cascade un programme de patient        (Sur Appel)        =#
#===========================================================================================#

def verifierSiProgramme(patient):
	if len(liste_programmes)>0:
		for programme in liste_programmes:
			if programme.getPatientProgramme() == patient:
				conn   = sqlite3.connect('NFB.db')
				cursor = conn.cursor()
				cursor.execute('''DELETE FROM Programmes_Table WHERE identifiant = (?)''',(patient.getIdentifiant(),))
				conn.commit()
				conn.close()	
				supprimerEnCascadeProgrammePatient(patient)

def supprimerEnCascadeProgrammePatient(patient):
	for programme in liste_programmes:
		if programme.getPatientProgramme()==patient:
			print("\nProgramme",programme.getIdentifiantProgramme()," supprimé\n")
			del programme
	rafraichirListes()


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-  	Vérifier et supprimer en cascade un entrainement de patient     (Choix 2d)         -*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def verifierSiEntrainement(patient):
	if len(liste_entrainements)>0:	
		for entrainement in liste_entrainements:
			if entrainement.getPatientEntrainement() == patient:
				conn   = sqlite3.connect('NFB.db')
				cursor = conn.cursor()
				cursor.execute('''DELETE FROM Entrainements_Table WHERE patient_id = (?)''',(patient.getIdentifiant(),))
				conn.commit()
				conn.close()	
				supprimerEnCascadeEntrainementPatient(patient)
	else:
		verifierSiProgramme(patient)

def supprimerEnCascadeEntrainementPatient(patient):
	for entrainement in liste_entrainements:
		if entrainement.getPatientEntrainement() == patient:
			print("\nEntrainement",entrainement.getIdentifiantEntrainement()," supprimé\n")
			del entrainement
			verifierSiProgramme(patient)
		else:
			verifierSiProgramme(patient)
	rafraichirListes()
	

#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#- 	Vérifier et supprimer en cascade un rapport de patient          (Choix 2e)         -*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def verifierSiRapport(patient):
	if len(liste_rapports)>0:
		for rapport in liste_rapports:
			if patient == rapport.getPatientRapport():
				conn   = sqlite3.connect('NFB.db')
				cursor = conn.cursor()
				cursor.execute('''DELETE FROM Rapports_Table WHERE rapport_id = (?)''',(patient.getIdentifiant(),))
				conn.commit()
				conn.close()
				rafraichirListes()
				supprimerEnCascadeRapportPatient(patient)
	else:
		verifierSiEntrainement(patient)

def supprimerEnCascadeRapportPatient(patient):
	for rapport in liste_rapports:
		if rapport.getPatientRapport() == patient:
			print("\nRapport #",rapport.getIdentifiantRapport()," supprimé\n")
			del rapport
			verifierSiEntrainement(patient)
		else:
			verifierSiEntrainement(patient)
	rafraichirListes()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER UN PATIENT                                             (Choix 130)        @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modification_patient')
def modifierPatientParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER UN PATIENT")
	print ("================================================================================\n")
	return render_template('130_modifier_patient.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE NOM D'UN PATIENT                                    (Choix 131)        @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_patient_nom')
def modifierPatientNomParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE NOM D'UN PATIENT")
	print ("================================================================================\n")
	patients_liste_X=formaterPatientHTML1()
	return render_template('131_modifier_patient_nom.html',patients=patients_liste_X)


@app.route('/modifier_patient_nom', methods=['POST'])
def modifierPatientNom():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE NOM D'UN PATIENT")
	try:
		identifiant = request.form["patient_id"]
		nouveau_nom = request.form["nouveau_nom"]
		if identifiant != "" and nouveau_nom != "" and testerExistencePatient(identifiant) == True:
			identifiant = int(identifiant)
			afficherUnPatient(identifiant)
			patients_liste_Y=formaterPatientHTML2(identifiant) # Afficher avant modification
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Patients_Table SET nom = (?) WHERE identifiant = (?)''',(nouveau_nom,identifiant))
			conn.commit()
			patient=objectiser_patient(identifiant)
			patient.setNom(nouveau_nom)
			rafraichirListes()
			afficherUnPatient(identifiant)
			patients_liste_Z=formaterPatientHTML2(identifiant) # Afficher après modification
			return render_template('/131_modifier_patient_nom.html', patient=patients_liste_Y,patientz=patients_liste_Z)
		else:
			entree_invalide
			return modifierPatientNomParDefaut()
		conn.close()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierPatientNomParDefaut()





#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE PRÉNOM D'UN PATIENT                                 (Choix 1-3-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_patient_prenom')
def modifierPatientPrenomParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE PRÉNOM D'UN PATIENT")
	print ("================================================================================\n")
	patients_liste_X=formaterPatientHTML1()
	return render_template('132_modifier_patient_prenom.html',patients=patients_liste_X)


@app.route('/modifier_patient_prenom', methods=['POST'])
def modifierPatientPrenom():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE PRÉNOM D'UN PATIENT")
	try:
		identifiant = request.form["patient_id"]
		nouveau_prenom = request.form["nouveau_prenom"]
		if identifiant != "" and nouveau_prenom != "" and testerExistencePatient(identifiant) == True:
			identifiant = int(identifiant)
			afficherUnPatient(identifiant)
			patients_liste_Y=formaterPatientHTML2(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Patients_Table SET prenom = (?) WHERE identifiant = (?)''',(nouveau_prenom,identifiant))
			conn.commit()
			patient=objectiser_patient(identifiant)
			patient.setPrenom(nouveau_prenom)
			rafraichirListes()
			afficherUnPatient(identifiant)
			patients_liste_Z=formaterPatientHTML2(identifiant)
			return render_template('/132_modifier_patient_prenom.html', patient=patients_liste_Y,patientz=patients_liste_Z)
		else:
			entree_invalide
			return modifierPatientPrenomParDefaut()
		conn.close()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierPatientPrenomParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER L'ÂGE D'UN PATIENT                                     (Choix 1-3-3)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_patient_age')
def modifierPatientAgeParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER L'ÂGE D'UN PATIENT")
	print ("================================================================================\n")
	patients_liste_X=formaterPatientHTML1()
	return render_template('133_modifier_patient_age.html',patients=patients_liste_X)


@app.route('/modifier_patient_age', methods=['POST'])
def modifierPatientAge():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER L'ÂGE D'UN PATIENT")
	try:
		identifiant = request.form["patient_id"]
		nouveau_age = request.form["nouveau_age"]
		if identifiant != "" and nouveau_age != "" and testerExistencePatient(identifiant) == True:
			identifiant = int(identifiant)
			afficherUnPatient(identifiant)
			patients_liste_Y=formaterPatientHTML2(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Patients_Table SET age = (?) WHERE identifiant = (?)''',(nouveau_age,identifiant))
			conn.commit()
			patient=objectiser_patient(identifiant)
			patient.setAgePatient(nouveau_age)
			rafraichirListes()
			afficherUnPatient(identifiant)
			patients_liste_Z=formaterPatientHTML2(identifiant)
			return render_template('/133_modifier_patient_age.html', patient=patients_liste_Y,patientz=patients_liste_Z)
		else:
			entree_invalide
			return modifierPatientAgeParDefaut()
		conn.close()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierPatientAgeParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE GENRE D'UN PATIENT                                  (Choix 1-3-4)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_patient_genre')
def modifierPatientGenreParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE GENRE D'UN PATIENT")
	print ("================================================================================\n")
	patients_liste_X=formaterPatientHTML1()
	return render_template('134_modifier_patient_genre.html',patients=patients_liste_X)


@app.route('/modifier_patient_genre', methods=['POST'])
def modifierPatientGenre():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE GENRE D'UN PATIENT")
	try:
		identifiant = request.form["patient_id"]
		nouveau_genre = request.form["nouveau_genre"]
		if identifiant != "" and nouveau_genre != "" and testerExistencePatient(identifiant) == True:
			identifiant = int(identifiant)
			afficherUnPatient(identifiant)
			patients_liste_Y=formaterPatientHTML2(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Patients_Table SET genre = (?) WHERE identifiant = (?)''',(nouveau_genre,identifiant))
			conn.commit()
			patient=objectiser_patient(identifiant)
			patient.setGenrePatient(nouveau_genre)
			rafraichirListes()
			afficherUnPatient(identifiant)
			patients_liste_Z=formaterPatientHTML2(identifiant)
			return render_template('/134_modifier_patient_genre.html', patient=patients_liste_Y,patientz=patients_liste_Z)
		else:
			entree_invalide
			return modifierPatientGenreParDefaut()
		conn.close()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierPatientGenreParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE CASQUE D'UN PATIENT                                  (Choix 1-3-5)     @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_patient_casque')
def modifierPatientCasqueParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE CASQUE D'UN PATIENT")
	print ("================================================================================\n")
	patients_liste_X=formaterPatientHTML1()
	return render_template('135_modifier_patient_casque.html',patients=patients_liste_X)


@app.route('/modifier_patient_casque', methods=['POST'])
def modifierPatientCasque():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE CASQUE D'UN PATIENT")
	try:
		identifiant = request.form["patient_id"]
		nouveau_casque = request.form["nouveau_casque"]
		if identifiant != "" and nouveau_casque != "" and testerExistencePatient(identifiant) == True:
			identifiant = int(identifiant)
			afficherUnPatient(identifiant)
			patients_liste_Y=formaterPatientHTML2(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Patients_Table SET casque = (?) WHERE identifiant = (?)''',(nouveau_casque,identifiant))
			conn.commit()
			patient=objectiser_patient(identifiant)
			patient.setCasquePatient(nouveau_casque)
			rafraichirPatient()
			afficherUnPatient(identifiant)
			patients_liste_Z=formaterPatientHTML2(identifiant)
			return render_template('/135_modifier_patient_casque.html', patient=patients_liste_Y,patientz=patients_liste_Z)
		else:
			entree_invalide
			return modifierPatientCasqueParDefaut()
		conn.close()
		rafraichirPatient()
	except ValueError:
		entree_invalide()
		return modifierPatientCasqueParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION PATIENT                                             (Choix 1-4-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/information_patient')
def informationPatientParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION PATIENT")
	print ("================================================================================\n")
	return render_template('140_info_patient.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES PATIENTS PAR ORDRE ALPHABÉTIQUE                       (Choix 1-4-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_patients_liste_np')
def afficherListePatientsNPParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES PATIENTS PAR ORDRE ALPHABÉTIQUE")
	print ("================================================================================\n")
	for patient in liste_patients:
		print("Identifiant :\t",patient.getIdentifiant(),"\nNom :\t\t",patient.getNom(),"\nPrénom :\t",patient.getPrenom(),"\nÂge :\t\t",patient.getAgePatient(),"\nGenre :\t\t",patient.getGenrePatient(),"\nCasque :\t",patient.getCasquePatient(),"\n\n")
	patients_liste_X=formaterPatientHTML1()
	return render_template('141_afficher_patients_liste_np.html', patients=patients_liste_X)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES PATIENTS PAR ORDRE CHRONOLOGIQUE                      (Choix 1-4-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_patients_liste_id')
def afficherListePatientsIDParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES PATIENTS PAR ORDRE CHRONOLOGIQUE")
	print ("================================================================================\n")
	patients_liste_X=formaterPatientChronoHTML()
	return render_template('142_afficher_patients_liste_id.html', patients=patients_liste_X)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION SUR UN PATIENT À L'AIDE D'UN IDENTIFIANT            (Choix 1-4-3)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_patient_id')
def afficherUnPatientIDParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION SUR UN PATIENT À L'AIDE D'UN IDENTIFIANT")
	print ("================================================================================\n")
	return render_template('143_afficher_un_patient_id.html')


@app.route('/afficher_un_patient_id', methods=['POST'])
def afficherUnPatientIDX():
	print ("\n\n--------------------------------------------------------------------------------\nINFORMATION SUR UN PATIENT À L'AIDE D'UN IDENTIFIANT")
	try:
		identifiant = request.form["patient_id"]
		afficherUnPatient(int(identifiant))
		conn   = sqlite3.connect('NFB.db')
		cursor = conn.cursor()
		patientBD=cursor.execute('''SELECT * FROM Patients_Table WHERE identifiant = (?)''',(identifiant,))
		return render_template('143_afficher_un_patient_id.html', patients=patientBD)
	except ValueError:
		entree_invalide()
		return afficherUnPatientIDParDefaut()
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION SUR UN PATIENT À L'AIDE D'UN NOM ET/OU PRÉNOM       (Choix 1-4-4)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_patient_np')
def afficherUnPatientNPParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION SUR UN PATIENT À L'AIDE D'UN NOM ET/OU PRÉNOM")
	print ("================================================================================\n")
	return render_template('144_afficher_un_patient_np.html')


@app.route('/afficher_un_patient_np', methods=['POST'])
def afficherUnPatientNPX():
	print ("\n\n--------------------------------------------------------------------------------\nINFORMATION SUR UN PATIENT À L'AIDE D'UN NOM ET/OU PRÉNOM")
	try:
		patientsBD=[]
		nom_web = request.form["nom"]
		prenom_web = request.form["prenom"]
		for patient in liste_patients:
			if patient.getNom().lower() == nom_web.lower() and patient.getPrenom().lower() == prenom_web.lower():
				print("\n\nIdentifiant :\t",patient.getIdentifiant(),"\nNom :\t\t",patient.getNom(),"\nPrénom :\t",patient.getPrenom(),"\nÂge :\t\t",patient.getAgePatient(),"\nGenre :\t\t",patient.getGenrePatient(),"\nCasque :\t",patient.getCasquePatient(),"\n\n")			
				patientx	= convertirPatientHTML(patient)
				patientsBD.append(patientx)
			if nom_web=="" or prenom_web=="":
				if patient.getNom().lower() == nom_web.lower() or patient.getPrenom().lower() == prenom_web.lower():
					print("\n\nIdentifiant :\t",patient.getIdentifiant(),"\nNom :\t\t",patient.getNom(),"\nPrénom :\t",patient.getPrenom(),"\nÂge :\t\t",patient.getAgePatient(),"\nGenre :\t\t",patient.getGenrePatient(),"\nCasque :\t",patient.getCasquePatient(),"\n\n")			
					patientx	= convertirPatientHTML(patient)
					patientsBD.append(patientx)
		return render_template('144_afficher_un_patient_np.html',patients=patientsBD)
	except ValueError:
		entree_invalide()
		return afficherUnPatientNPParDefaut()

def convertirPatientHTML(patient):
	identifiant	= patient.getIdentifiant()
	nom             = patient.getNom()
	prenom          = patient.getPrenom()
	age             = patient.getAgePatient()
	genre           = patient.getGenrePatient()
	casque          = patient.getCasquePatient()			
	patientx	= [identifiant,nom,prenom,age,genre,casque]	
	return patientx


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@#=                                                                                       =#@
#@#=                                FONCTIONS ENTRAINEUR                                   =#@
#@#=                                                                                       =#@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#===========================================================================================#
# 	Mise à jour des entraîneurs                                    (Sur Appel)         =#
#===========================================================================================#

def rafraichirEntraineur():
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute(''' SELECT * FROM Entraineurs_Table ORDER BY nom ASC,prenom ASC''')
	lignesTableEntraineur = cursor.fetchall()
	conn.close()
	del liste_entraineurs [:]
	for attribut in lignesTableEntraineur:
		identifiant             = attribut[0]
		nom                     = attribut[1]
		prenom                  = attribut[2]
		entraineur              = Entraineur(identifiant,nom,prenom)
		liste_entraineurs.append(entraineur)


#===========================================================================================#
#= 	Vérifier si un entraineur existe                                                   =#
#===========================================================================================#

def testerExistenceEntraineur(identifiant):
	existence = []
	for entraineur in liste_entraineurs:
		if str(identifiant) == str(entraineur.getIdentifiant()):
			existence.append(entraineur.getIdentifiant())
	if len(existence)!=0:
		return True
	else:
		return False


#===========================================================================================#
#=	Formater entraineur pour HTML                                  (Sur Appel)         =#
#===========================================================================================#

def formaterEntraineurHTML1():
	entraineurs_liste_X = []
	for entraineur in liste_entraineurs:
		entraineurx=(entraineur.getIdentifiant(),entraineur.getNom(),entraineur.getPrenom())
		entraineurs_liste_X.append(entraineurx)
	return entraineurs_liste_X


def formaterEntraineurChronoHTML():
	entraineurs_liste_X = []
	for entraineur in liste_entraineurs:
		entraineurx=(entraineur.getIdentifiant(),entraineur.getNom(),entraineur.getPrenom())
		entraineurs_liste_X.append(entraineurx)
	entraineurs_liste_X.sort()
	for item in entraineurs_liste_X:
		print("Identifiant :\t",item[0],"\nNom :\t\t",item[1],"\nPrénom :\t",item[2],"\n\n")
	return entraineurs_liste_X


def formaterEntraineurHTML2(entraineur_id):
	entraineurs_liste_Y = []
	entraineur = objectiser_entraineur(entraineur_id)
	entraineury=(entraineur.getIdentifiant(),entraineur.getNom(),entraineur.getPrenom())
	entraineurs_liste_Y.append(entraineury)
	return entraineurs_liste_Y


#===========================================================================================#
#= 	Terminal : Afficher un entraineur à l'aide de son identifiant  (Sur Appel)         =#
#===========================================================================================#

def afficherUnEntraineur(identifiant):
	for entraineur in liste_entraineurs:
		if entraineur.getIdentifiant() == identifiant:
			print("\nIdentifiant :\t",entraineur.getIdentifiant(),"\nNom :\t\t",entraineur.getNom(),"\nPrénom :\t",entraineur.getPrenom(),"\n\n")



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MENU ENTRAINEUR                                                (Choix 2-0-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/menu_entraineur')
def menuEntraineurParDefaut():
	print ("\n\n================================================================================")
	print("MENU ENTRAINEUR")
	print ("================================================================================\n")
	return render_template('200_menu_entraineur.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AJOUTER UN ENTRAINEUR                                           (Choix 2-1-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/ajouter_entraineur')
def ajouterEntraineurParDefaut():
	print ("\n\n================================================================================")
	print("AJOUTER UN ENTRAINEUR")
	print ("================================================================================\n")
	entraineurs_liste_X=formaterEntraineurHTML1()
	return render_template('210_ajouter_entraineur.html', entraineurs=entraineurs_liste_X)


@app.route ('/ajouter_entraineur', methods=['POST'])
def ajouterEntraineur():
	print ("\n\n--------------------------------------------------------------------------------\nAJOUTER UN ENTRAINEUR")
	entrainements_liste_Y = []
	try:
		identifiant=genererIDEntraineur()+1
		nom                             = request.form["nom"]
		prenom                          = request.form["prenom"]
		if nom != "" and prenom != "" :
			entraineur              = Entraineur(identifiant,nom,prenom)
			ajout_Entraineur_BD(entraineur)
			entraineury             = (entraineur.getIdentifiant(),entraineur.getNom(),entraineur.getPrenom())
			entrainements_liste_Y.append(entraineury)
			return render_template('210_ajouter_entraineur.html', entraineur=entrainements_liste_Y)	
		else:
			entree_invalide()
			return ajouterEntraineurParDefaut()
	except ValueError:
		entree_invalide()
		return ajouterEntraineurParDefaut()	


def genererEntraineur1():
	identifiant=genererIDEntraineur()
	if identifiant == 0:
		identifiant=genererIDEntraineur()+1
		entraineur = Entraineur(identifiant,"Entraineur","par défaut")
		conn   = sqlite3.connect('NFB.db')
		cursor = conn.cursor()
		cursor.execute('''INSERT INTO Entraineurs_Table(identifiant,nom,prenom) VALUES(?,?,?)''',(entraineur.getIdentifiant(),entraineur.getNom(),entraineur.getPrenom()))
		conn.commit()
		conn.close()
		rafraichirListes()	


def genererIDEntraineur():
	rafraichirEntraineur()
	ID=[]
	for entraineur in liste_entraineurs:
		ID.append(entraineur.getIdentifiant())
	try:
		return max(ID)
	except ValueError:
		return 0


def ajout_Entraineur_BD(entraineur):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute('''INSERT INTO Entraineurs_Table(identifiant,nom,prenom) VALUES(?,?,?)''',(entraineur.getIdentifiant(),entraineur.getNom(),entraineur.getPrenom()))
	conn.commit()
	conn.close()
	print("\n"+ entraineur.__str__() + " ajouté\n")
	rafraichirListes()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	SUPPRIMER UN ENTRAINEUR                                         (Choix 2-2-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/supprimer_entraineur')
def supprimerUnEntraineurParDefaut():
	print ("\n\n================================================================================")
	print("SUPPRIMER UN ENTRAINEUR")
	print ("================================================================================\n")
	entraineurs_liste_X=formaterEntraineurHTML1()
	return render_template('220_supprimer_entraineur.html', entraineurs=entraineurs_liste_X)


@app.route ('/supprimer_entraineur', methods=['POST'])
def supprimerEntraineurBD():
	print ("\n\n--------------------------------------------------------------------------------\nSUPPRIMER UN ENTRAINEUR")
	try:
		identifiant = request.form["entraineur_id"]
		identifiant = int(identifiant)
		if identifiant != "" and testerExistenceEntraineur(identifiant) == True :
			entraineurs_liste_Y=formaterEntraineurHTML2(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' DELETE FROM Entraineurs_Table WHERE identifiant  = (?)''',(identifiant,))
			conn.commit()
			supprimerObjetEntraineur(identifiant)
			rafraichirListes()
			return render_template('220_supprimer_entraineur.html', entraineur=entraineurs_liste_Y)
		else:
			entree_invalide
			return supprimerUnEntraineurParDefaut()
		conn.close()
	except ValueError:
		entree_invalide()
		return supprimerUnEntraineurParDefaut()


#===========================================================================================#
#= 	Supprimer un objet entraineur                                                      =#
#===========================================================================================#


def supprimerObjetEntraineur(identifiant):
	for entraineur in liste_entraineurs:
		if entraineur.getIdentifiant() == identifiant:
			print("\n\n"+ entraineur.__str__() + " supprimé\n")
			print("Effacement des données\n\n")
			del entraineur		
	rafraichirListes()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER UN ENTRAINEUR                                          (Choix 2-3-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modification_entraineur')
def modifierEntraineurParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER ENTRAINEUR")
	print ("================================================================================\n")
	return render_template('230_modifier_entraineur.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE NOM D'UN ENTRAINEUR                                 (Choix 2-3-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_entraineur_nom')
def modifierEntraineurNomParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE NOM D'UN ENTRAINEUR")
	print ("================================================================================\n")
	entraineurs_liste_X=formaterEntraineurHTML1()
	return render_template('231_modifier_entraineur_nom.html',entraineurs=entraineurs_liste_X)


@app.route('/modifier_entraineur_nom', methods=['POST'])
def modifierEntraineurNom():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE NOM D'UN ENTRAINEUR ")
	try:
		identifiant = request.form["entraineur_id"]
		nouveau_nom = request.form["nouveau_nom"]
		if identifiant != "" and nouveau_nom != "" and testerExistenceEntraineur(identifiant) == True :
			identifiant = int(identifiant)
			afficherUnEntraineur(identifiant)
			entraineurs_liste_Y=formaterEntraineurHTML2(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Entraineurs_Table SET nom = (?) WHERE identifiant = (?)''',(nouveau_nom,identifiant))
			conn.commit()
			entraineur=objectiser_entraineur(identifiant)
			entraineur.setNom(nouveau_nom)
			rafraichirListes()
			afficherUnEntraineur(identifiant)
			entraineurs_liste_Z=formaterEntraineurHTML2(identifiant)
			return render_template('231_modifier_entraineur_nom.html',entraineur=entraineurs_liste_Y,entraineurz=entraineurs_liste_Z)
		else:
			entree_invalide
			return modifierEntraineurNomParDefaut()
		conn.close()
	except ValueError:
		entree_invalide()
		return modifierEntraineurNomParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE PRÉNOM D'UN ENTRAINEUR                              (Choix 2-3-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_entraineur_prenom')
def modifierEntraineurPrenomParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE PRÉNOM D'UN ENTRAINEUR")
	print ("================================================================================\n")
	entraineurs_liste_X=formaterEntraineurHTML1()
	return render_template('232_modifier_entraineur_prenom.html',entraineurs=entraineurs_liste_X)


@app.route('/modifier_entraineur_prenom', methods=['POST'])
def modifierEntraineurPrenom():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE PRÉNOM D'UN ENTRAINEUR")
	try:
		identifiant = request.form["entraineur_id"]
		nouveau_prenom = request.form["nouveau_prenom"]
		if identifiant != "" and nouveau_prenom != "" and testerExistenceEntraineur(identifiant) == True :
			identifiant = int(identifiant)
			afficherUnEntraineur(identifiant)
			entraineurs_liste_Y=formaterEntraineurHTML2(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Entraineurs_Table SET prenom = (?) WHERE identifiant = (?)''',(nouveau_prenom,identifiant))
			conn.commit()
			entraineur=objectiser_entraineur(identifiant)
			entraineur.setPrenom(nouveau_prenom)
			rafraichirListes()
			afficherUnEntraineur(identifiant)
			entraineurs_liste_Z=formaterEntraineurHTML2(identifiant)
			return render_template('232_modifier_entraineur_prenom.html',entraineur=entraineurs_liste_Y,entraineurz=entraineurs_liste_Z)
		else:
			entree_invalide
			return modifierEntraineurPrenomParDefaut()
		conn.close()
	except ValueError:
		entree_invalide()
		return modifierEntraineurPrenomParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION ENTRAINEUR                                          (Choix 2-4-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/information_entraineur')
def informationEntraineurParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION ENTRAINEUR")
	print ("================================================================================\n")
	return render_template('240_info_entraineur.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES ENTRAINEURS PAR ORDRE ALPHABÉTIQUE                    (Choix 2-4-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_entraineurs_liste_np')
def afficherListeEntraineursNPParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES ENTRAINEURS PAR ORDRE ALPHABÉTIQUE")
	print ("================================================================================\n")
	for entraineur in liste_entraineurs:
		print("Identifiant :\t",entraineur.getIdentifiant(),"\nNom :\t\t",entraineur.getNom(),"\nPrénom :\t",entraineur.getPrenom(),"\n\n")
	entraineurs_liste_X=formaterEntraineurHTML1()
	return render_template('241_afficher_entraineurs_liste_np.html', entraineurs=entraineurs_liste_X)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES ENTRAINEURS PAR ORDRE CROISSANT D'IDENTIFIANTS        (Choix 2-4-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_entraineurs_liste_id')
def afficherListeEntraineursIDParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES ENTRAINEURS PAR ORDRE CROISSANT D'IDENTIFIANTS")
	print ("================================================================================\n")
	entraineurs_liste_X=formaterEntraineurChronoHTML()
	return render_template('242_afficher_entraineurs_liste_id.html', entraineurs=entraineurs_liste_X)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION SUR UN ENTRAINEUR À L'AIDE D'UN IDENTIFIANT         (Choix 2-4-3)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_entraineur_id')
def afficherUnEntraineurIDParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION SUR UN ENTRAINEUR À L'AIDE D'UN IDENTIFIANT")
	print ("================================================================================\n")
	return render_template('243_afficher_un_entraineur_id.html')


@app.route('/afficher_un_entraineur_id', methods=['POST'])
def afficherUnEntraineurIDX():
	print ("\n\n--------------------------------------------------------------------------------\nINFORMATION SUR UN ENTRAINEUR À L'AIDE D'UN IDENTIFIANT")
	try:
		identifiant = request.form["entraineur_id"]
		afficherUnEntraineur(int(identifiant))
		conn   = sqlite3.connect('NFB.db')
		cursor = conn.cursor()
		entraineurs_liste_X=cursor.execute('''SELECT * FROM Entraineurs_Table WHERE identifiant = (?)''',(identifiant,))
		return render_template('243_afficher_un_entraineur_id.html', entraineurs=entraineurs_liste_X)
	except ValueError:
		entree_invalide()
		return afficherUnEntraineurIDParDefaut()
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION SUR UN ENTRAINEUR À L'AIDE D'UN NOM ET/OU PRÉNOM    (Choix 2-4-4)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_entraineur_np')
def afficherUnEntraineurNPParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION SUR UN ENTRAINEUR À L'AIDE D'UN NOM ET/OU PRÉNOM")
	print ("================================================================================\n")
	return render_template('244_afficher_un_entraineur_np.html')


@app.route('/afficher_un_entraineur_np', methods=['POST'])
def afficherUnEntraineurNPX():
	print ("\n\n--------------------------------------------------------------------------------\nINFORMATION SUR UN ENTRAINEUR À L'AIDE D'UN NOM ET/OU PRÉNOM")
	try:
		entraineurs_liste_X=[]
		nom_web = request.form["nom"]
		prenom_web = request.form["prenom"]
		for entraineur in liste_entraineurs:
			if entraineur.getNom().lower() == nom_web.lower() and entraineur.getPrenom().lower() == prenom_web.lower():
				print("\n\nIdentifiant :\t",entraineur.getIdentifiant(),"\nNom :\t\t",entraineur.getNom(),"\nPrénom :\t",entraineur.getPrenom(),"\n\n")			
				entraineurx	= convertirEntraineurHTML(entraineur)
				entraineurs_liste_X.append(entraineurx)
			if nom_web=="" or prenom_web=="":
				if entraineur.getNom().lower() == nom_web.lower() or entraineur.getPrenom().lower() == prenom_web.lower():
					print("\n\nIdentifiant :\t",entraineur.getIdentifiant(),"\nNom :\t\t",entraineur.getNom(),"\nPrénom :\t",entraineur.getPrenom(),"\n\n")			
					entraineurx	= convertirEntraineurHTML(entraineur)
					entraineurs_liste_X.append(entraineurx)
		return render_template('244_afficher_un_entraineur_np.html',entraineurs=entraineurs_liste_X)
	except ValueError:
		entree_invalide()
		return afficherUnEntraineurNPParDefaut()


def convertirEntraineurHTML(entraineur):
	identifiant	= entraineur.getIdentifiant()
	nom             = entraineur.getNom()
	prenom          = entraineur.getPrenom()		
	entraineurx	= [identifiant,nom,prenom]	
	return entraineurx




#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@#=                                                                                       =#@
#@#=                                FONCTIONS PROTOCOLE                                    =#@
#@#=                                                                                       =#@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#===========================================================================================#
#= 	Mise à jour des protocoles                                     (Sur Appel)         =#
#===========================================================================================#

def rafraichirProtocole():
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute(''' SELECT * FROM Protocoles_Table ORDER BY nom_protocole ASC''')
	lignesTableProtocole = cursor.fetchall()
	conn.close()
	del liste_protocoles [:]
	for attribut in lignesTableProtocole:
		protocole_id            = attribut[0]
		nom_protocole           = attribut[1]
		aires_broadmann         = attribut[2]
		protocole               = Protocole(protocole_id,nom_protocole,aires_broadmann)
		liste_protocoles.append(protocole)


#===========================================================================================#
#= 	Vérifier si un protocole existe                                                    =#
#===========================================================================================#

def testerExistenceProtocole(protocole_id):
	existence = []
	for protocole in liste_protocoles:
		if str(protocole_id) == str(protocole.getIdentifiantProtocole()):
			existence.append(protocole.getIdentifiantProtocole())
	if len(existence)!=0:
		return True
	else:
		return False


#===========================================================================================#
#=	Formater protocole pour HTML                                   (Sur Appel)         =#
#===========================================================================================#

def formaterProtocoleHTML1():
	protocoles_liste_X = []
	for protocole in liste_protocoles:
		protocolex=(protocole.getIdentifiantProtocole(),protocole.getNomProtocole(),protocole.getAiresBroadmannProtocole())
		protocoles_liste_X.append(protocolex)
	return protocoles_liste_X

def formaterProtocoleHTML2(protocole_id):
	protocoles_liste_Y = []
	protocole = objectiser_protocole(protocole_id)
	protocoley=(protocole.getIdentifiantProtocole(),protocole.getNomProtocole(),protocole.getAiresBroadmannProtocole())
	protocoles_liste_Y.append(protocoley)
	return protocoles_liste_Y


#===========================================================================================#
#= 	Terminal : Afficher un protocole à l'aide de son identifiant   (Sur Appel)         =#
#===========================================================================================#

def afficherUnProtocole(protocole_id):
	for protocole in liste_protocoles:
		if protocole.getIdentifiantProtocole() == protocole_id:
			print("\nIdentifiant :\t",protocole.getIdentifiantProtocole(),"\nNom :\t\t",protocole.getNomProtocole(),"\nRégions :\t",protocole.getAiresBroadmannProtocole(),"\n\n")



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MENU PROTOCOLE                                                  (Choix 3-0-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/menu_protocole')
def menuProtocoleParDefaut():
	print ("\n\n================================================================================")
	print("MENU PROTOCOLE")
	print ("================================================================================\n")
	return render_template('300_menu_protocole.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AJOUTER UN PROTOCOLE                                            (Choix 3-1-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/ajouter_protocole')
def ajouterProtocoleParDefaut():
	print ("\n\n================================================================================")
	print("AJOUTER UN PROTOCOLE")
	print ("================================================================================\n")
	for protocole in liste_protocoles:
		print("Identifiant :\t",protocole.getIdentifiantProtocole(),"\nNom :\t\t",protocole.getNomProtocole(),"\nRégions :\t",protocole.getAiresBroadmannProtocole(),"\n\n")
	protocoles_liste_X=formaterProtocoleHTML1()
	return render_template('310_ajouter_protocole.html', protocoles=protocoles_liste_X)


@app.route ('/ajouter_protocole', methods=['POST'])
def ajouterProtocoleX():
	print ("\n\n--------------------------------------------------------------------------------\nAJOUTER UN PROTOCOLE")
	protocoles_liste_Y = []
	protocole_id=genererIDProtocole()+1
	nom_protocole = request.form["nom"]
	aires_broadmann = request.form["regions"]
	if nom_protocole != "" and aires_broadmann != "" :
		protocole = Protocole(protocole_id,nom_protocole,aires_broadmann)
		ajout_Protocole_BD(protocole)
		rafraichirListes()
		protocoles_liste_Y=formaterProtocoleHTML2(protocole_id)
		return render_template('310_ajouter_protocole.html', protocole=protocoles_liste_Y)
	else:
		entree_invalide()
		return ajouterProtocoleParDefaut()
		

def genererIDProtocole():
	rafraichirProtocole()
	ID=[]
	for protocole in liste_protocoles:
		ID.append(protocole.getIdentifiantProtocole())
	try:
		return max(ID)
	except ValueError:
		return 0


def ajout_Protocole_BD(protocole):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute('''INSERT INTO Protocoles_Table(protocole_id,nom_protocole,aires_broadmann) VALUES(?,?,?)''',(protocole.getIdentifiantProtocole(),protocole.getNomProtocole(),protocole.getAiresBroadmannProtocole()))
	conn.commit()
	conn.close()
	print("\n"+ protocole.__str__() + " ajouté\n")
	rafraichirListes()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	SUPPRIMER UN PROTOCOLE                                          (Choix 3-2-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/supprimer_protocole')
def supprimerUnProtocoleParDefaut():
	print ("\n\n================================================================================")
	print("SUPPRIMER UN PROTOCOLE À L'AIDE D'UN IDENTIFIANT")
	print ("================================================================================\n")
	protocoles_liste_X=formaterProtocoleHTML1()
	return render_template('320_supprimer_protocole.html', protocoles=protocoles_liste_X)


@app.route ('/supprimer_protocole', methods=['POST'])
def supprimerProtocoleX():
	print ("\n\n--------------------------------------------------------------------------------\nSUPPRIMER UN PROTOCOLE")
	try:
		protocole_id = request.form["protocole_id"]
		if protocole_id != "" and testerExistenceProtocole(protocole_id) == True :
			protocole_id = int(protocole_id)
			afficherUnProtocole(protocole_id)
			protocoles_liste_Y=formaterProtocoleHTML2(protocole_id)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' DELETE FROM Protocoles_Table WHERE protocole_id  = (?)''',(protocole_id,))
			conn.commit()
			supprimerObjetProtocole(protocole_id)
			rafraichirListes()
			return render_template('320_supprimer_protocole.html', protocole=protocoles_liste_Y)
		else:
			entree_invalide
			return supprimerUnProtocoleParDefaut()
		conn.close()
	except ValueError:
		entree_invalide()
		return supprimerUnProtocoleParDefaut()



#===========================================================================================#
#= 	Supprimer un objet protocole                                                       =#
#===========================================================================================#

def supprimerObjetProtocole(protocole_id):
	for protocole in liste_protocoles:
		if protocole.getIdentifiantProtocole() == protocole_id:
			print(protocole.__str__() + " supprimé\n")
			print("Effacement des données\n\n")
			del protocole		
	rafraichirListes()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER UN PROTOCOLE                                           (Choix 3-3-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modification_protocole')
def modifierProtocoleParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER UN PROTOCOLE")
	print ("================================================================================\n")
	return render_template('330_modifier_protocole.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE NOM D'UN PROTOCOLE                                  (Choix 3-3-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_protocole_nom')
def modifierProtocoleNomParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE NOM D'UN PROTOCOLE")
	print ("================================================================================\n")
	protocoles_liste_X=formaterProtocoleHTML1()
	return render_template('331_modifier_protocole_nom.html',protocoles=protocoles_liste_X)


@app.route('/modifier_protocole_nom', methods=['POST'])
def modifierProtocoleNomX():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE NOM D'UN PROTOCOLE")
	try:
		protocole_id = request.form["protocole_id"]
		nouveau_nom_protocole = request.form["nouveau_nom_protocole"]
		if protocole_id != "" and nouveau_nom_protocole != "" and testerExistenceProtocole(protocole_id) == True :
			protocole_id = int(protocole_id)
			afficherUnProtocole(protocole_id)
			protocoles_liste_Y=formaterProtocoleHTML2(protocole_id)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Protocoles_Table SET nom_protocole = (?) WHERE protocole_id = (?)''',(nouveau_nom_protocole,protocole_id))
			conn.commit()
			protocole=objectiser_protocole(protocole_id)
			protocole.setNomProtocole(nouveau_nom_protocole)
			rafraichirListes()
			afficherUnProtocole(protocole_id)
			protocoles_liste_Z=formaterProtocoleHTML2(protocole_id)
			return render_template('331_modifier_protocole_nom.html',protocole=protocoles_liste_Y,protocolez=protocoles_liste_Z)
		else:
			entree_invalide
			return modifierProtocoleNomParDefaut()
		conn.close()
	except ValueError:
		entree_invalide()
		return modifierProtocoleNomParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LES RÉGIONS D'UN PROTOCOLE                             (Choix 3-3-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_protocole_regions')
def modifierProtocoleRegionsParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LES RÉGIONS D'UN PROTOCOLE")
	print ("================================================================================\n")
	protocoles_liste_X=formaterProtocoleHTML1()
	return render_template('332_modifier_protocole_regions.html',protocoles=protocoles_liste_X)


@app.route('/modifier_protocole_regions', methods=['POST'])
def modifierProtocoleRegionsX():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LES RÉGIONS D'UN PROTOCOLE")
	try:
		protocole_id = request.form["protocole_id"]
		nouveau_aires_broadmann = request.form["nouveau_aires_broadmann"]
		if protocole_id != "" and nouveau_aires_broadmann != "" and testerExistenceProtocole(protocole_id) == True :
			protocole_id = int(protocole_id)
			afficherUnProtocole(protocole_id)
			protocoles_liste_Y=formaterProtocoleHTML2(protocole_id)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()	
			cursor.execute(''' UPDATE Protocoles_Table SET aires_broadmann = (?) WHERE protocole_id = (?)''',(nouveau_aires_broadmann,protocole_id))
			conn.commit()
			protocole=objectiser_protocole(protocole_id)
			protocole.setAiresBroadmannProtocole(nouveau_aires_broadmann)
			rafraichirListes()
			afficherUnProtocole(protocole_id)
			protocoles_liste_Z=formaterProtocoleHTML2(protocole_id)
			return render_template('332_modifier_protocole_regions.html',protocole=protocoles_liste_Y,protocolez=protocoles_liste_Z)
		else:
			entree_invalide
			return modifierProtocoleRegionsParDefaut()
		conn.close()
	except ValueError:
		entree_invalide()
		return modifierProtocoleRegionsParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION PROTOCOLE                                           (Choix 3-4-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/information_protocole')
def informationProtocoleParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION PROTOCOLE")
	print ("================================================================================\n")
	return render_template('340_info_protocole.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES PROTOCOLES PAR ORDRE ALPHABÉTIQUE                     (Choix 3-4-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_protocoles_liste_np')
def afficherListeProtocolesNPParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES PROTOCOLES PAR ORDRE ALPHABÉTIQUE")
	print ("================================================================================\n")
	for protocole in liste_protocoles:
		print("Identifiant :\t",protocole.getIdentifiantProtocole(),"\nNom :\t\t",protocole.getNomProtocole(),"\nRégions :\t",protocole.getAiresBroadmannProtocole(),"\n\n")
	protocoles_liste_X=formaterProtocoleHTML1()
	return render_template('341_afficher_protocoles_liste_np.html', protocoles=protocoles_liste_X)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES PROTOCOLES PAR ORDRE CHRONOLOGIQUE                    (Choix 3-4-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_protocoles_liste_id')
def afficherListeProtocolesIDParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES PROTOCOLES PAR ORDRE CHRONOLOGIQUE")
	print ("================================================================================\n")
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	protocoles_liste_X=cursor.execute(''' SELECT * FROM Protocoles_Table ''')
	for attributs in protocoles_liste_X:
		print("Identifiant :\t",attributs[0],"\nNom :\t\t",attributs[1],"\nRégions :\t",attributs[2],"\n\n")
	protocoles_liste_X=cursor.execute(''' SELECT * FROM Protocoles_Table ''')
	return render_template('342_afficher_protocoles_liste_id.html', protocoles=protocoles_liste_X)


@app.route('/afficher_protocoles_liste_id', methods=['POST'])
def afficherListeProtocolesIDX():
	print ("\n\n--------------------------------------------------------------------------------\nLISTE DES PROTOCOLES PAR ORDRE CHRONOLOGIQUE")
	protocoles_liste_X = []
	try:
		conn   = sqlite3.connect('NFB.db')
		cursor = conn.cursor()
		protocoles_liste_X=cursor.execute(''' SELECT * FROM Protocoles_Table ORDER BY protocole_id ASC ''')
		return render_template('342_afficher_protocoles_liste_id.html', protocoles=protocoles_liste_X)
	except sqlite3.Error as e:
		print (e)
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION SUR UN PROTOCOLE À L'AIDE D'UN IDENTIFIANT          (Choix 3-4-3)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_protocole_id')
def afficherUnProtocoleIDParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION SUR UN PROTOCOLE À L'AIDE D'UN IDENTIFIANT")
	print ("================================================================================\n")
	return render_template('343_afficher_un_protocole_id.html')


@app.route('/afficher_un_protocole_id', methods=['POST'])
def afficherUnProtocoleIDX():
	print ("\n\n--------------------------------------------------------------------------------\nINFORMATION SUR UN PROTOCOLE À L'AIDE D'UN IDENTIFIANT")
	try:
		protocole_id = request.form["protocole_id"]
		afficherUnProtocole(int(protocole_id))
		conn   = sqlite3.connect('NFB.db')
		cursor = conn.cursor()
		protocoles_liste_X=cursor.execute('''SELECT * FROM Protocoles_Table WHERE protocole_id = (?)''',(protocole_id,))
		return render_template('343_afficher_un_protocole_id.html', protocoles=protocoles_liste_X)
	except ValueError:
		entree_invalide()
		return afficherUnPatientIDParDefaut()
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION SUR UN PROTOCOLE À L'AIDE D'UN NOM                  (Choix 3-4-4)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_protocole_np')
def afficherUnProtocoleNPParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION SUR UN PROTOCOLE À L'AIDE D'UN NOM")
	print ("================================================================================\n")
	return render_template('344_afficher_un_protocole_np.html')


@app.route('/afficher_un_protocole_np', methods=['POST'])
def afficherUnProtocoleNPX():
	print ("\n\n--------------------------------------------------------------------------------\nINFORMATION SUR UN PROTOCOLE À L'AIDE D'UN NOM")
	try:
		protocoles_liste_X=[]
		nom_web = request.form["nom"]
		for protocole in liste_protocoles:
			if protocole.getNomProtocole().lower() == nom_web.lower():
				print("\n\nIdentifiant :\t",protocole.getIdentifiantProtocole(),"\nNom :\t\t",protocole.getNomProtocole(),"\nRégions :\t",protocole.getAiresBroadmannProtocole(),"\n\n")			
				protocolex	= convertirProtocoleHTML(protocole)
				protocoles_liste_X.append(protocolex)
		return render_template('344_afficher_un_protocole_np.html',protocoles=protocoles_liste_X)
	except ValueError:
		entree_invalide()
		return afficherUnProtocoleNPParDefaut()


def convertirProtocoleHTML(protocole):
	identifiant	= protocole.getIdentifiantProtocole()
	nom             = protocole.getNomProtocole()
	regions         = protocole.getAiresBroadmannProtocole()
	protocolex	= [identifiant,nom,regions]	
	return protocolex



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@#=                                                                                       =#@
#@#=                                FONCTIONS PROGRAMME                                    =#@
#@#=                                                                                       =#@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#===========================================================================================#
#= 	Mise à jour des programmes                                     (Sur Appel)         =#
#===========================================================================================#

def rafraichirProgramme():
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute(''' SELECT * FROM Programmes_Table ORDER BY nom ASC,prenom ASC,priorite_clinique ASC''')
	lignesTableProgramme = cursor.fetchall()
	conn.close()
	del liste_programmes [:]
	for attribut in lignesTableProgramme:
		programme_id            = attribut[0]
		patient_id              = attribut[1]
		protocole_id            = attribut[2]
		priorite_clinique       = attribut[3]
		nombre_seances          = attribut[4]
		nom                     = attribut[5]
		prenom                  = attribut[6]
		rafraichirProgramme2(programme_id,patient_id,protocole_id,priorite_clinique,nombre_seances,nom,prenom)

def rafraichirProgramme2(programme_id,patient_id,protocole_id,priorite_clinique,nombre_seances,nom,prenom):
	for patient in liste_patients:
		if patient.getIdentifiant() == patient_id:
				for protocole in liste_protocoles:
					if protocole.getIdentifiantProtocole() == protocole_id:
						programme = Programme(programme_id,patient,protocole,priorite_clinique,nombre_seances,nom,prenom)
						liste_programmes.append(programme)


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Mise à jour des rapports suite à une modification de programme  (Sur Appel)        -* 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def actualiserProgRapport(programme_id):
	for programme in liste_programmes:
		if str(programme.getIdentifiantProgramme()) == str(programme_id):
			patient = programme.getPatientProgramme()
			if len(liste_rapports) != 0:
				identifiant=patient.getIdentifiant()
				actualiserProgRapport2(identifiant)

def actualiserProgRapport2(identifiant):
	for rapport in liste_rapports:
		if identifiant == rapport.getIdentifiantRapport():
			patient= objectiser_patient(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()
			cursor.execute('''DELETE FROM Rapports_Table WHERE rapport_id = (?)''',(identifiant,))
			conn.commit()
			conn.close()
			rafraichirListes()
			ajouterObjetRapport(identifiant,patient,patient.getNom(),patient.getPrenom())


#===========================================================================================#
#= 	Vérifier si un programme existe                                                    =#
#===========================================================================================#

def testerExistenceProgramme(programme_id):
	existence = []
	for programme in liste_programmes:
		if programme_id == programme.getIdentifiantProgramme():
			existence.append(programme.getIdentifiantProgramme())
	if len(existence)!=0:
		return True
	else:
		return False


#===========================================================================================#
#=	Formater programme pour HTML                                   (Sur Appel)         =#
#===========================================================================================#

def convertirProgrammeHTML(programme_id):
	programme                       = objectiser_programme(programme_id)
	programme_id                    = programme.getIdentifiantProgramme()
	programme_patient               = (str(programme.getPatientProgramme().getNom()) + " " + str(programme.getPatientProgramme().getPrenom()))
	patient_id                      = programme.getPatientProgramme().getIdentifiant()
	protocole_nom                   = programme.getProtocoleProgramme().getNomProtocole()
	protocole_id                    = programme.getProtocoleProgramme().getIdentifiantProtocole()
	priorite                        = programme.getPrioriteCliniqueProgramme()
	seances                         = programme.getNombreSeancesProgramme()
	protocole_regions               = programme.getProtocoleProgramme().getAiresBroadmannProtocole()
	programmey                      = (programme_id,programme_patient,patient_id,protocole_nom,protocole_id,priorite,seances,protocole_regions)
	return programmey


def formaterProgrammeHTML1():
	programmes_liste_X = []
	for programme in liste_programmes:
		programme_id            = programme.getIdentifiantProgramme()
		programmex              = convertirProgrammeHTML(programme_id)
		programmes_liste_X.append(programmex)
	return programmes_liste_X


def formaterProgrammeChronoHTML():
	programmes_liste_X = []
	for programme in liste_programmes:
		programme_id            = programme.getIdentifiantProgramme()
		programmex              = convertirProgrammeHTML(programme_id)
		programmes_liste_X.append(programmex)
	programmes_liste_X.sort()
	return programmes_liste_X


def formaterProgrammeHTML2(programme_id):
	programmes_liste_Y = []
	programmey                      = convertirProgrammeHTML(programme_id)
	programmes_liste_Y.append(programmey)
	return programmes_liste_Y


#===========================================================================================#
#= 	TERMINAL : Afficher les programmes selon un ordre donné        (Sur Appel)         =#
#===========================================================================================#

def afficherProgrammesDansTerminal(programmes):
	print ('{:<20}{:<5}{:<22}{:<6}'.format(" PROGRAMME"," PATIENT","","PROTOCOLE"),"\n")
	print ('{:<6}{:<16}'.format(""," Priorité"))
	print ('{:<6}{:<14}{:<6}{:<23}{:<6}{:<17}'.format(" ID"," & Séances"," ID"," Nom & Prénom"," ID"," Nom"),"\n")
	for programme in programmes:
		programme_id                    = programme.getIdentifiantProgramme()
		patient_id                      = programme.getPatientProgramme().getIdentifiant()
		nom_prenom                      = str(programme.getPatientProgramme().getNom())+" "+str(programme.getPatientProgramme().getPrenom())
		protocole_id                    = programme.getProtocoleProgramme().getIdentifiantProtocole()
		protocole_nom                   = programme.getProtocoleProgramme().getNomProtocole()
		priorite_clinique               = programme.getPrioriteCliniqueProgramme()
		nombre_seances                  = programme.getNombreSeancesProgramme()
		print (' {:<6}{:<6}{:<8}{:<6}{:<23}{:<6}{:<17}'.format(programme_id,priorite_clinique,nombre_seances,patient_id,nom_prenom,protocole_id,protocole_nom))
	print ("\n\n\n")


#===========================================================================================#
#= 	Terminal : Afficher un programme à l'aide de son identifiant   (Sur Appel)         =#
#===========================================================================================#

def afficherProgrammesAvecID(programme_id):
	for programme in liste_programmes:
		if programme.getIdentifiantProgramme() == programme_id:
			print ("Programme #" + str(programme.getIdentifiantProgramme()))
			print (programme.getPatientProgramme())
			print (programme,"\n")



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MENU PROGRAMME                                                  (Choix 4-0-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/menu_programme')
def menuProgrammeParDefaut():
	print ("\n\n================================================================================")
	print("MENU PROGRAMME")
	print ("================================================================================\n")
	return render_template('400_menu_programme.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AJOUTER UN PROGRAMME                                            (Choix 4-1-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/ajouter_programme')
def ajouterProgrammeParDefaut():
	print ("\n\n================================================================================")
	print("AJOUTER UN PROGRAMME")
	print ("================================================================================\n")
	programmes_liste_X = formaterProgrammeHTML1()
	afficherProgrammesDansTerminal(liste_programmes)
	return render_template('410_ajouter_programme.html', programmes=programmes_liste_X)


@app.route ('/ajouter_programme', methods=['POST'])
def ajouterProgrammeX():
	print ("\n\n--------------------------------------------------------------------------------\nAJOUTER UN PROGRAMME\n")
	try:
		programme_id            = genererIDProgramme()+1
		identifiant             = request.form["patient_id"]
		protocole_id            = request.form["protocole_id"]
		priorite_clinique       = request.form["priorite_clinique"]
		nombre_seances          = request.form["nombre_seances"]
		identifiant             = int(identifiant)
		protocole_id            = int(protocole_id)
		priorite_clinique       = int(priorite_clinique)
		nombre_seances          = int(nombre_seances)
		patient                 = objectiser_patient(identifiant)
		protocole               = objectiser_protocole(int(protocole_id))
		if patient != None and protocole != None and priorite_clinique != "" and nombre_seances != "" :
			programme = Programme(programme_id,patient,protocole,priorite_clinique,nombre_seances,patient.getNom(),patient.getPrenom())
			ajouter_Programme_BD(programme,patient,protocole,patient.getNom(),patient.getPrenom())
			afficherProgrammesAvecID(programme_id)
			print("\nProgramme ajouté\n\n")
			programmes_liste_Y = formaterProgrammeHTML2(programme_id)
			actualiserProgRapport(programme_id)
			return render_template('410_ajouter_programme.html', programme=programmes_liste_Y)
		else:
			entree_invalide()
			return ajouterProgrammeParDefaut()
	except ValueError:
		entree_invalide()
		return ajouterProgrammeParDefaut()		


#===========================================================================================#
#= 	Déterminer l'identifiant du dernier programme créé                                 =#
#===========================================================================================#

def genererIDProgramme():
	ID=[]
	for programme in liste_programmes:
		ID.append(programme.getIdentifiantProgramme())
	try:
		return max(ID)
	except ValueError:
		return 0


#===========================================================================================#
#= 	Ajouter un programme dans la base de donnée                                        =#
#===========================================================================================#

def ajouter_Programme_BD(programme,patient,protocole,nom,prenom):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute('''INSERT INTO Programmes_Table(programme_id,identifiant,protocole_id,priorite_clinique,nombre_seances,nom,prenom) VALUES(?,?,?,?,?,?,?)''',(programme.getIdentifiantProgramme(),patient.getIdentifiant(),protocole.getIdentifiantProtocole(),programme.getPrioriteCliniqueProgramme(),programme.getNombreSeancesProgramme(),nom,prenom))
	conn.commit()
	conn.close()
	programme_id=programme.getIdentifiantProgramme()
	rafraichirProgramme()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	SUPPRIMER UN PROGRAMME                                          (Choix 4-2-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/supprimer_programme')
def supprimerUnProgrammeParDefaut():
	print ("\n\n================================================================================")
	print("SUPPRIMER UN PROGRAMME")
	print ("================================================================================\n")
	programmes_liste_X = formaterProgrammeHTML1()
	afficherProgrammesDansTerminal(liste_programmes)
	return render_template('420_supprimer_programme.html', programmes=programmes_liste_X)


@app.route ('/supprimer_programme', methods=['POST'])
def supprimerProgrammeX():
	print ("\n\n--------------------------------------------------------------------------------\nSUPPRIMER UN PROGRAMME\n")
	try:
		programme_id = request.form["programme_id"]
		programme_id = int(programme_id)
		if programme_id != "" and testerExistenceProgramme(programme_id) == True :
			programmes_liste_Y = formaterProgrammeHTML2(programme_id)
			afficherProgrammesAvecID(programme_id)
			programme=objectiser_programme(programme_id)
			identifiant=programme.getPatientProgramme().getIdentifiant()	
			supprimerObjetProgramme(programme_id)			
			supprimer_Programme_BD(programme_id)
			actualiserProgRapport2(identifiant)
			rafraichirListes()
			return render_template('420_supprimer_programme.html', programme=programmes_liste_Y)
		else:
			entree_invalide
			return supprimerUnProgrammeParDefaut()
		conn.close()
		rafraichirProgramme()
	except ValueError:
		entree_invalide()
		return supprimerUnProgrammeParDefaut()


#===========================================================================================#
#= 	Supprimer un objet programme                                                       =#
#===========================================================================================#

def supprimerObjetProgramme(programme_id):
	for programme in liste_programmes:
		if programme.getIdentifiantProgramme() == programme_id:
			print("\n\n"+ programme.__str__() + " supprimé\n")
			print("Effacement des données\n\n")
			del programme		


#===========================================================================================#
#= 	Supprimer un programme de la base de donnée                                        =#
#===========================================================================================#

def supprimer_Programme_BD(programme_id):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' DELETE FROM Programmes_Table WHERE programme_id  = (?)''',(programme_id,))
	conn.commit()
	conn.close()
	rafraichirProgramme()




#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER UN PROGRAMME                                           (Choix 4-3-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modification_programme')
def modifierProgrammeParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER UN PROGRAMME")
	print ("================================================================================\n")
	return render_template('430_modifier_programme.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE PATIENT D'UN PROGRAMME                              (Choix 4-3-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_programme_patient')
def modifierProgrammePatientParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE PATIENT D'UN PROGRAMME")
	print ("================================================================================\n")
	programmes_liste_X = formaterProgrammeHTML1()
	afficherProgrammesDansTerminal(liste_programmes)
	return render_template('431_modifier_programme_patient.html',programmes=programmes_liste_X)


@app.route('/modifier_programme_patient', methods=['POST'])
def modifierProgrammePatientX():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE PATIENT D'UN PROGRAMME\n")
	try:
		programme_id 			= request.form["programme_id"]
		programme_id                    = int(programme_id)
		programme                       = objectiser_programme(programme_id)
		identifiant                     = request.form["patient_id"]
		identifiant                     = int(identifiant)
		if testerExistenceProgramme(programme_id) == True and testerExistencePatient(identifiant) == True :
			afficherProgrammesAvecID(programme_id)
			programmes_liste_Y              = formaterProgrammeHTML2(programme_id)	
			patient                         = objectiser_patient(identifiant)
			programme                       = objectiser_programme(programme_id)
			vieux_identifiant               = programme.getPatientProgramme().getIdentifiant()
			actualiserProgRapport2(vieux_identifiant)
			nouveau_nom                     = patient.getNom()
			nouveau_prenom                  = patient.getPrenom()
			modifierProgrammePatient(programme,patient,identifiant,nouveau_nom,nouveau_prenom)
			rafraichirProgramme()
			afficherProgrammesAvecID(programme_id)
			programmes_liste_Z              = formaterProgrammeHTML2(programme_id)	
			actualiserProgRapport2(identifiant)
			rafraichirListes()
			return render_template('431_modifier_programme_patient.html',programme=programmes_liste_Y, programmez=programmes_liste_Z)
		else:
			entree_invalide
			return modifierProgrammePatientParDefaut()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierProgrammePatientParDefaut()

	
#===========================================================================================#
#= 	Modifier le patient d'un programme                                                 =#
#===========================================================================================#

def modifierProgrammePatient(programme,patient,identifiant,nouveau_nom,nouveau_prenom):
	programme_id=programme.getIdentifiantProgramme()
	programme.setPatientProgramme(patient)
	programme.setNomProgramme(nouveau_nom)
	programme.setPrenomProgramme(nouveau_prenom)
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' UPDATE Programmes_Table SET identifiant = (?) WHERE programme_id = (?)''',(identifiant,programme_id))
	cursor.execute(''' UPDATE Programmes_Table SET nom = (?) WHERE programme_id = (?)''',(nouveau_nom,programme_id))
	cursor.execute(''' UPDATE Programmes_Table SET prenom = (?) WHERE programme_id = (?)''',(nouveau_prenom,programme_id))
	conn.commit()
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE PROTOCOLE D'UN PROGRAMME                            (Choix 4-3-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_programme_protocole')
def modifierProgrammeProtocoleParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE PROTOCOLE D'UN PROGRAMME")
	print ("================================================================================\n")
	programmes_liste_X = formaterProgrammeHTML1()
	afficherProgrammesDansTerminal(liste_programmes)
	return render_template('432_modifier_programme_protocole.html',programmes=programmes_liste_X)


@app.route('/modifier_programme_protocole', methods=['POST'])
def modifierProgrammeProtocoleX():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE PROTOCOLE D'UN PROGRAMME\n")
	try:
		programme_id 			= request.form["programme_id"]
		programme_id                    = int(programme_id)
		programme                       = objectiser_programme(programme_id)
		protocole_id                    = request.form["protocole_id"]
		protocole_id                    = int(protocole_id)
		if testerExistenceProgramme(programme_id) == True and testerExistenceProtocole(protocole_id) == True :
			programme                       = objectiser_programme(programme_id)
			identifiant                     = programme.getPatientProgramme().getIdentifiant()
			afficherProgrammesAvecID(programme_id)
			programmes_liste_Y=formaterProgrammeHTML2(programme_id)	
			protocole                       = objectiser_protocole(protocole_id)
			modifierProgrammeProtocole(programme,protocole)
			rafraichirProgramme()
			afficherProgrammesAvecID(programme_id)
			programmes_liste_Z=formaterProgrammeHTML2(programme_id)	
			actualiserProgRapport2(identifiant)
			rafraichirListes()
			return render_template('432_modifier_programme_protocole.html',programme=programmes_liste_Y, programmez=programmes_liste_Z)
		else:
			entree_invalide
			return modifierProgrammeProtocoleParDefaut()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierProgrammeProtocoleParDefaut()


#===========================================================================================#
#= 	Modifier le protocole d'un programme                                               =#
#===========================================================================================#

def modifierProgrammeProtocole(programme,protocole):
	programme_id=programme.getIdentifiantProgramme()
	protocole_id=protocole.getIdentifiantProtocole()
	programme.setProtocoleProgramme(protocole)
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' UPDATE Programmes_Table SET protocole_id = (?) WHERE programme_id = (?)''',(protocole_id,programme_id))
	conn.commit()
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LA PRIORITÉ CLINIQUE D'UN PROGRAMME                    (Choix 4-3-3)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_programme_priorite')
def modifierProgrammePrioriteParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER PROGRAMME PRIORITÉ CLINIQUE")
	print ("================================================================================\n")
	programmes_liste_X = formaterProgrammeHTML1()
	afficherProgrammesDansTerminal(liste_programmes)
	return render_template('433_modifier_programme_priorite.html',programmes=programmes_liste_X)


@app.route('/modifier_programme_priorite', methods=['POST'])
def modifierProgrammePrioriteX():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER PROGRAMME PRIORITÉ CLINIQUE\n")
	try:
		programme_id 			= request.form["programme_id"]
		programme_id                    = int(programme_id)
		programme                       = objectiser_programme(programme_id)
		priorite_clinique               = request.form["priorite_clinique"]
		priorite_clinique               = int(priorite_clinique)
		if testerExistenceProgramme(programme_id) == True  and priorite_clinique != "":
			programme                       = objectiser_programme(programme_id)
			identifiant                     = programme.getPatientProgramme().getIdentifiant()
			afficherProgrammesAvecID(programme_id)
			programmes_liste_Y=formaterProgrammeHTML2(programme_id)
			modifierProgrammePriorite(programme,priorite_clinique)
			rafraichirProgramme()
			afficherProgrammesAvecID(programme_id)
			programmes_liste_Z=formaterProgrammeHTML2(programme_id)
			actualiserProgRapport(identifiant)
			rafraichirListes
			return render_template('433_modifier_programme_priorite.html',programme=programmes_liste_Y, programmez=programmes_liste_Z)
		else:
			entree_invalide
			return modifierProgrammePrioriteParDefaut()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierProgrammePrioriteParDefaut()


#===========================================================================================#
#= 	Modifier la priorité clinique d'un programme                                       =#
#===========================================================================================#

def modifierProgrammePriorite(programme,priorite_clinique):
	programme_id=programme.getIdentifiantProgramme()
	programme.setPrioriteCliniqueProgramme(priorite_clinique)
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' UPDATE Programmes_Table SET priorite_clinique = (?) WHERE programme_id = (?)''',(priorite_clinique,programme_id))
	conn.commit()
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE NOMBRE DE SÉANCES D'ENTRAINEMENTS D'UN PROGRAMME    (Choix 4-3-4)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_programme_seances')
def modifierProgrammeSeancesParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER PROGRAMME NOMBRE DE SÉANCES")
	print ("================================================================================\n")
	programmes_liste_X = formaterProgrammeHTML1()
	afficherProgrammesDansTerminal(liste_programmes)
	return render_template('434_modifier_programme_seances.html',programmes=programmes_liste_X)


@app.route('/modifier_programme_seances', methods=['POST'])
def modifierProgrammeSeancesX():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER PROGRAMME NOMBRE DE SÉANCES\n")
	try:
		programme_id 			= request.form["programme_id"]
		programme_id                    = int(programme_id)
		programme                       = objectiser_programme(programme_id)
		nombre_seances                  = request.form["nombre_seances"]
		nombre_seances                  = int(nombre_seances)
		if testerExistenceProgramme(programme_id) == True  and nombre_seances != "":
			programme                       = objectiser_programme(programme_id)
			identifiant                     = programme.getPatientProgramme().getIdentifiant()
			afficherProgrammesAvecID(programme_id)
			programmes_liste_Y=formaterProgrammeHTML2(programme_id)
			modifierProgrammeSeances(programme,nombre_seances)
			rafraichirProgramme()
			afficherProgrammesAvecID(programme_id)
			programmes_liste_Z=formaterProgrammeHTML2(programme_id)
			actualiserProgRapport(identifiant)
			rafraichirListes()
			return render_template('434_modifier_programme_seances.html',programme=programmes_liste_Y,programmez=programmes_liste_Z)
		else:
			entree_invalide
			return modifierProgrammeSeancesParDefaut()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierProgrammeSeancesParDefaut()



#===========================================================================================#
#= 	Modifier un programme dans la BD                                (Sur Appel)        =#
#===========================================================================================#

def modifierProgrammeSeances(programme,nombre_seances):
	programme_id=programme.getIdentifiantProgramme()
	programme.setNombreSeancesProgramme(nombre_seances)
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' UPDATE Programmes_Table SET nombre_seances = (?) WHERE programme_id = (?)''',(nombre_seances,programme_id))
	conn.commit()
	conn.close()
	modifierProgrammeSeances2(programme,nombre_seances)

def modifierProgrammeSeances2(programme,nombre_seances):
	identifiant=programme.getPatientProgramme().getIdentifiant()
	if testerExistenceRapport(identifiant)==True:
		rapport                         = objectiser_rapport(identifiant)
		patient                         = objectiser_patient(identifiant)
		priorite_programme              = classerProgrammesPatient(patient)       # Données A
		programmes_patient,entrainements_requis=classerProgrammesPatient2(priorite_programme)
		rapport.setEntRequisRapport(entrainements_requis)
		conn   = sqlite3.connect('NFB.db')
		cursor = conn.cursor()	
		cursor.execute(''' UPDATE Rapports_Table SET entrainements_requis = (?) WHERE rapport_id = (?)''',(entrainements_requis,identifiant))
		conn.commit()
		conn.close()
		rafraichirListes()	
	



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION PROGRAMME                                           (Choix 4-4-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/information_programme')
def informationProgrammeParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION PROGRAMME")
	print ("================================================================================\n")
	return render_template('440_info_programme.html')


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES PROGRAMMES PAR ORDRE ALPHABÉTIQUE ET PAR PRIORITÉ     (Choix 4-4-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_programmes_liste_np')
def afficherListeProgrammesNPParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES PROGRAMMES PAR ORDRE ALPHABÉTIQUE ET PAR PRIORITÉ")
	print ("================================================================================\n")
	programmes_liste_X = formaterProgrammeHTML1()
	afficherProgrammesDansTerminal(liste_programmes)	
	return render_template('441_afficher_programmes_liste_np.html', programmes=programmes_liste_X)


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES PROGRAMMES PAR ORDRE CHRONOLOGIQUE                    (Choix 4-4-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_programmes_liste_id')
def afficherListeProgrammesIDParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES PROGRAMMES PAR ORDRE CHRONOLOGIQUE")
	print ("================================================================================\n")
	programmes_liste_X=formaterProgrammeChronoHTML()
	programmes_liste_Z=[]
	programmes_liste_Y=[]
	for programme in liste_programmes:
		programmez=[programme.getIdentifiantProgramme(),programme]
		programmes_liste_Z.append(programmez)
	programmes_liste_Z.sort()
	for id_programme in programmes_liste_Z:
		programme = id_programme[1]
		programmes_liste_Y.append(programme)
	afficherProgrammesDansTerminal(programmes_liste_Y)
	return render_template('442_afficher_programmes_liste_id.html', programmes=programmes_liste_X)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AFFICHER UN PROGRAMME À L'AIDE D'UN IDENTIFIANT PROGRAMME       (Choix 4-4-3)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_programme_id')
def afficherUnProgrammeIDParDefaut():
	print ("\n\n================================================================================")
	print("AFFICHER UN PROGRAMME À L'AIDE D'UN IDENTIFIANT PROGRAMME")
	print ("================================================================================\n")
	return render_template('443_afficher_un_programme_id.html')


@app.route('/afficher_un_programme_id', methods=['POST'])
def afficherUnProgrammeIDX():
	print ("\n\n--------------------------------------------------------------------------------\nAFFICHER UN PROGRAMME À L'AIDE D'UN IDENTIFIANT PROGRAMME\n")
	try:
		programme_id 			= request.form["programme_id"]
		programme_id                    = int(programme_id)
		programmex                      = objectiser_programme(programme_id)
		if programmex != None and programme_id != "":
			for programme in liste_programmes:
				if programme.getIdentifiantProgramme()==programme_id:
					afficherProgrammesAvecID(programme_id)	
					programmes_liste_Y = formaterProgrammeHTML2(programme_id)	
			return render_template('443_afficher_un_programme_id.html', programmes=programmes_liste_Y)
		else:
			entree_invalide()
			return afficherUnProgrammeIDParDefaut()
	except ValueError:
		entree_invalide()
		return afficherUnProgrammeIDParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AFFICHER LES PROGRAMMES D'UN PATIENT                            (Choix 4-4-4)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_programme_np')
def afficherUnProgrammeNPParDefaut():
	print ("\n\n================================================================================")
	print("AFFICHER LES PROGRAMMES D'UN PATIENT")
	print ("================================================================================\n")
	return render_template('444_afficher_un_programme_np.html')


@app.route('/afficher_un_programme_np', methods=['POST'])
def afficherUnProgrammeNPX():
	print ("\n\n--------------------------------------------------------------------------------\nAFFICHER LES PROGRAMMES D'UN PATIENT\n")
	try: 
		nom = request.form["nom"]
		prenom  = request.form["prenom"]
		identifiant = request.form["identifiant"]
		if identifiant != "":
			identifiant = int(identifiant)
			patient=objectiser_patient(identifiant)
			if patient != None:
				programmes_liste_Y=classerProgrammesAvecIDPatient(patient)
				return render_template('444_afficher_un_programme_np.html',programmes=programmes_liste_Y)
			else:
				return afficherUnProgrammeNPParDefaut()
		elif nom != "" or prenom != "":
			programmes_liste_X=classerProgrammesAvecNPPatient(nom,prenom)
			return render_template('444_afficher_un_programme_np.html',programmes=programmes_liste_X)
		else:
			return afficherUnProgrammeNPParDefaut()
	except ValueError:
		return afficherUnProgrammeNPParDefaut()

def classerProgrammesAvecIDPatient(patient):
	programmes_liste_X=[]
	for programme in liste_programmes:
		if programme.getPatientProgramme() == patient:
			afficherProgrammesAvecID(programme.getIdentifiantProgramme())
			programmey=convertirProgrammeHTML(programme.getIdentifiantProgramme())
			programmes_liste_X.append(programmey)
	return programmes_liste_X


def classerProgrammesAvecNPPatient(nom,prenom):
	programmes_liste_Z=[]
	programmes_liste_X=[]
	for patient in liste_patients :
		if len(liste_patients)  == 0 :
			print("\nAucun patient\n")
		elif patient.getNom().lower() == nom.lower() and patient.getPrenom().lower() == prenom.lower():	
			for programme in liste_programmes:
				if programme.getPatientProgramme() == patient :
					afficherProgrammesAvecID(programme.getIdentifiantProgramme())
					programmes_liste_Z.append(programme)
		if nom == "" or prenom == "":
			if patient.getNom().lower() == nom.lower() or patient.getPrenom().lower() == prenom.lower():	
				for programme in liste_programmes:
					if programme.getPatientProgramme() == patient :	
						afficherProgrammesAvecID(programme.getIdentifiantProgramme())
						afficherProgrammesAvecID(programme.getIdentifiantProgramme())
						programmes_liste_Z.append(programme)
	for programme in programmes_liste_Z:
		programme_id = programme.getIdentifiantProgramme()
		programmex=convertirProgrammeHTML(programme.getIdentifiantProgramme())		
		programmes_liste_X.append(programmex)
	return programmes_liste_X




#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@#=                                                                                       =#@
#@#=                               FONCTIONS ENTRAINEMENT                                  =#@
#@#=                                                                                       =#@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#===========================================================================================#
#= 	Mise à jour des entraînements                                  (Sur Appel)         =#
#===========================================================================================#

def rafraichirEntrainement():
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute(''' SELECT * FROM Entrainements_Table ORDER BY date ASC,heure ASC''')
	lignesTableEntrainement=cursor.fetchall()
	conn.close()
	del liste_entrainements [:]
	for attribut in lignesTableEntrainement:
		entrainement_id         = attribut[0]
		date                    = attribut[1]
		heure                   = attribut[2]
		patient_id              = attribut[3]
		entraineur_id           = attribut[4]
		nom                     = attribut[5]
		prenom                  = attribut[6]
		rafraichirEntrainement2(entrainement_id,date,heure,patient_id,entraineur_id,nom,prenom)


def rafraichirEntrainement2(entrainement_id,date,heure,patient_id,entraineur_id,nom,prenom):
	for patient in liste_patients:
		if patient.getIdentifiant() == patient_id:
				for entraineur in liste_entraineurs:
					if entraineur.getIdentifiant() == entraineur_id:
						entrainement = Entrainement(entrainement_id,date,heure,patient,entraineur,nom,prenom)
						liste_entrainements.append(entrainement)


#===========================================================================================#
#= 	Vérifier si un entrainement existe                                                 =#
#===========================================================================================#

def testerExistenceEntrainement(entrainement_id):
	existence = []
	for entrainement in liste_entrainements:
		if str(entrainement_id) == str(entrainement.getIdentifiantEntrainement()):
			existence.append(entrainement.getIdentifiantEntrainement())
	if len(existence)!=0:
		return True
	else:
		return False


#===========================================================================================#
#= 	Formater entrainements pour HTML                                                   =#
#===========================================================================================#

def formaterEntrainementHTML(entrainement_id):
	entrainement    = objectiser_entrainement(entrainement_id)
	date            = entrainement.getDateEntrainement()
	heure           = str(entrainement.getHeureEntrainement())+":00"
	patient_nom     = entrainement.getPatientEntrainement().getNom()
	patient_prenom  = entrainement.getPatientEntrainement().getPrenom()
	patient_id      = entrainement.getPatientEntrainement().getIdentifiant()
	entraineur_nom  = entrainement.getEntraineurEntrainement().getNom()
	entraineur_prenom  = entrainement.getEntraineurEntrainement().getPrenom()
	entraineur_id   = entrainement.getEntraineurEntrainement().getIdentifiant()
	compte          = compterEntrainementHTML(entrainement_id)
	entrainementz   = (entrainement_id,date,heure,patient_nom,patient_prenom,patient_id,entraineur_nom,entraineur_prenom,entraineur_id,compte)
	return entrainementz


def formaterEntrainementHTML1():
	entrainements_liste_X = []
	for entrainement in liste_entrainements:
		entrainement_id = entrainement.getIdentifiantEntrainement()
		entrainementx=formaterEntrainementHTML(entrainement_id)		
		entrainements_liste_X.append(entrainementx)		
	entrainements_liste_X.sort()
	return entrainements_liste_X


def formaterEntrainementHTML2(entrainement_id):
	entrainements_liste_Y =[]
	entrainement=objectiser_entrainement(entrainement_id)
	entrainementy=formaterEntrainementHTML(entrainement_id)
	entrainements_liste_Y.append(entrainementy)
	return entrainements_liste_Y


def compterEntrainementHTML(entrainement_id):
	entrainements_patient = []
	compte = 0
	entrainement    = objectiser_entrainement(entrainement_id)
	patient_id      = entrainement.getPatientEntrainement().getIdentifiant()
	for entrainement in liste_entrainements:
		if entrainement.getPatientEntrainement().getIdentifiant()==patient_id:
			compte += 1
			entrainement_id2 = entrainement.getIdentifiantEntrainement()
			compte_id2 = (compte,entrainement_id2)
			entrainements_patient.append(compte_id2)
	for compte_id2 in entrainements_patient:
		if compte_id2 [1] == entrainement_id :
			compte = compte_id2 [0]
			if compte == 1:
				compte = str(compte) + "er"
			else:
				compte = str(compte) + "e"
			return compte


#===========================================================================================#
#= 	TERMINAL : Afficher les entrainements selon un ordre donné        (Sur Appel)      =#
#===========================================================================================#

def afficherEntrainementsDansTerminal(entrainements):
	print ("\n\n"+'{:<15}{:<20}{:<28}{:<16}'.format(" ENTRAINEMENT"," PLAGE HORAIRE"," PATIENT"," ENTRAINEUR")+'{:<15}{:<20}{:<28}{:<16}'.format("  ID","  Date & Heure","  ID,  Nom & Prénom","  Nom & Prénom")+"\n")
	for entrainement in liste_entrainements:
		patient                 = entrainement.getPatientEntrainement()
		patient_id              = patient.getIdentifiant()
		nom_prenom              = str(patient.getNom())+" "+str(patient.getPrenom())
		date                    = entrainement.getDateEntrainement()
		heure                   = entrainement.getHeureEntrainement()
		entraineur              = entrainement.getEntraineurEntrainement()
		nom_prenom2             = str(entraineur.getNom())+" "+str(entraineur.getPrenom())
		print (' {:<15}{:<12}{:<8}{:<4}{:<24}{:<15}'.format(entrainement.getIdentifiantEntrainement(),date,heure,patient_id,nom_prenom,nom_prenom2))
	print ("\n\n")


#===========================================================================================#
#=  	Terminal Liste des entraînement par ordre chronologique          (Sur Appel)       =#
#===========================================================================================#
def afficherLesEntrainements():
	print ("\n\n"+'{:<15}{:<20}{:<28}{:<16}'.format(" ENTRAINEMENT"," PLAGE HORAIRE"," PATIENT"," ENTRAINEUR")+'{:<15}{:<20}{:<28}{:<16}'.format("  ID","  Date & Heure","  ID,  Nom & Prénom","  Nom & Prénom")+"\n")
	for entrainement in liste_entrainements:
		patient                 = entrainement.getPatientEntrainement()
		patient_id              = patient.getIdentifiant()
		nom_prenom              = str(patient.getNom())+" "+str(patient.getPrenom())
		date                    = entrainement.getDateEntrainement()
		heure                   = entrainement.getHeureEntrainement()
		entraineur              = entrainement.getEntraineurEntrainement()
		nom_prenom2             = str(entraineur.getNom())+" "+str(entraineur.getPrenom())
		print (' {:<15}{:<12}{:<8}{:<4}{:<24}{:<15}'.format(entrainement.getIdentifiantEntrainement(),date,heure,patient_id,nom_prenom,nom_prenom2))
	print("\n\n\n")


#===========================================================================================#
#= 	Terminal : Afficher un entrainement à l'aide d'un identifiant     (Sur Appel)      =#
#===========================================================================================#

def afficherEntrainementsAvecID(entrainement_id):
	for entrainement in liste_entrainements:
		if entrainement.getIdentifiantEntrainement() == entrainement_id:
			print ("Entrainement #" + str(entrainement.getIdentifiantEntrainement()))
			print (entrainement.getPatientEntrainement())
			print (entrainement,"\n")


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MENU ENTRAINEMENT                                               (Choix 5-0-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/menu_entrainement')
def menuEntrainmentParDefaut():
	print ("\n\n================================================================================")
	print("MENU ENTRAINEMENT")
	print ("================================================================================\n")
	return render_template('500_menu_entrainement.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AJOUTER UN ENTRAINEMENT                                         (Choix 5-1-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/ajouter_entrainement')
def ajouterEntrainementParDefaut():
	print ("\n\n================================================================================")
	print("AJOUTER UN ENTRAINEMENT")
	print ("================================================================================\n")
	entrainements_liste_X = formaterEntrainementHTML1()
	afficherEntrainementsDansTerminal(liste_entrainements)
	return render_template('510_ajouter_entrainement.html', entrainements=entrainements_liste_X)


@app.route ('/ajouter_entrainement', methods=['POST'])
def ajouterEntrainementX():
	print ("\n\n--------------------------------------------------------------------------------\nAJOUTER UN ENTRAINEMENT\n")
	date		        = request.form["date"]
	heure		        = request.form["heure"]
	patient_id              = request.form["patient_id"]
	entraineur_id           = request.form["entraineur_id"]
	if date != "" and heure != "" and patient_id != "" and entraineur_id != "":
		try:
			heure                   = int(heure)
			if heure <24 and len(date)==10:
				if heure <10:
					heure = "0"+ str(heure) 
				annee           = date[0]+date[1]+date[2]+date[3]
				mois            = date[5]+date[6]
				jour            = date[8]+date[9]
				date                    = date[0]+date[1]+date[2]+date[3]+"-"+date[5]+date[6]+"-"+date[8]+date[9]
				entrainement_id         = str(annee)+str(mois)+str(jour)+str(heure)
				entrainement_id         = int(entrainement_id)
				if testerExistenceEntrainement(entrainement_id) == False:
					patient_id              = int(patient_id)
					entraineur_id           = int(entraineur_id)
					patient                 = objectiser_patient(patient_id)
					entraineur              = objectiser_entraineur(entraineur_id)
					if patient != None and entraineur != None :
						entrainement    = Entrainement(entrainement_id,date,heure,patient,entraineur,patient.getNom(),patient.getPrenom())
						ajouter_Entrainement_BD(entrainement,entrainement_id,date,heure,patient_id,entraineur_id,patient.getNom(),patient.getPrenom())
						print(entrainement)
						print("\n\n\t\t\tEntrainement " + str(entrainement_id) + " ajouté\n\n\n\n")
						entrainements_liste_Y = formaterEntrainementHTML2(entrainement.getIdentifiantEntrainement())	
						return render_template('510_ajouter_entrainement.html',entrainement=entrainements_liste_Y)
					else:
						invalide = ["Patient et / ou Entraîneur invalide"]
						message_erreur = messageErreur(invalide)
						return render_template('510_ajouter_entrainement.html',message_erreur=message_erreur)
				else:
					invalide = ["Cette plage horaire est occupée"]
					message_erreur = messageErreur(invalide)
					return render_template('510_ajouter_entrainement.html',message_erreur=message_erreur)
			else:
				invalide = ["Date et / ou Heure invalide"]
				message_erreur = messageErreur(invalide)
				return render_template('510_ajouter_entrainement.html',message_erreur=message_erreur)
		except ValueError:
			invalide = ["Heure invalide"]
			message_erreur = messageErreur(invalide)
			return render_template('510_ajouter_entrainement.html',message_erreur=message_erreur)
	else:
		entree_invalide()
		return ajouterEntrainementParDefaut()


#===========================================================================================#
#= 	Vérifier la disponilité d'une plage horaire                     (Sur appel)        =# 
#===========================================================================================#


def testerExistencePlageHoraire(entrainement_id):
	existence = []
	for entrainement in liste_entrainements:
		if entrainement_id == entrainement.getIdentifiantEntrainement():
			existence.append(entrainement.getIdentifiantEntrainement())
	if len(existence)!=0:
		return True
	else:
		return False


#===========================================================================================#
#= 	Enregistrer un entraînement (BD)                                (Sur appel)        =#  
#===========================================================================================#

def ajouter_Entrainement_BD(entrainement,entrainement_id,date,heure,patient_id,entraineur_id,nom,prenom):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute('''INSERT INTO Entrainements_Table(entrainement_id,date,heure,patient_id,entraineur_id,nom,prenom) VALUES(?,?,?,?,?,?,?)''',(entrainement_id,date,heure,patient_id,entraineur_id,nom,prenom))
	conn.commit()
	conn.close()
	rafraichirListes()
	actualiserEntrainementRapport2(patient_id)
#	actualisation_Entrainement_Rapport(entrainement)


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Ajouter un entraînement dans un rapport existant                (Sur appel)        -*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def actualisation_Entrainement_Rapport(entrainement):
	rapport_id = entrainement.getPatientEntrainement().getIdentifiant()
	if len(liste_rapports) != 0:
		for rapport in liste_rapports:
			if rapport_id == rapport.getIdentifiantRapport():
				actualiserEntrainementRapport2(rapport_id)
				rafraichirListes()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	SUPPRIMER UN ENTRAINEMENT                                       (Choix 5-2-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/supprimer_entrainement')
def supprimerUnEntrainementParDefaut():
	print ("\n\n================================================================================")
	print("SUPPRIMER UN ENTRAINEMENT")
	afficherEntrainementsDansTerminal(liste_entrainements)
	entrainements_liste_X=formaterEntrainementHTML1()
	return render_template('520_supprimer_entrainement.html', entrainements=entrainements_liste_X)


@app.route ('/supprimer_entrainement', methods=['POST'])
def supprimerEntrainementX():
	print ("\n\n--------------------------------------------------------------------------------\nSUPPRIMER UN ENTRAINEMENT")
	try:
		entrainement_id = request.form["entrainement_id"]
		if entrainement_id != "":
			entrainement_id = int(entrainement_id)
			if testerExistenceEntrainement(entrainement_id) == True:
				entrainement=objectiser_entrainement(entrainement_id)
				patient_id = entrainement.getPatientEntrainement().getIdentifiant()
				entrainements_liste_Y = formaterEntrainementHTML2(entrainement_id)
				afficherEntrainementsAvecID(entrainement_id)				
				supprimer_entrainement_BD(entrainement_id)
				supprimerObjetEntrainement(entrainement_id)
				rafraichirListes()
				actualiserEntrainementRapport2(patient_id)
				return render_template('520_supprimer_entrainement.html', entrainement=entrainements_liste_Y)
			else:
				entree_invalide()
				return supprimerUnEntrainementParDefaut()
		else:
			entree_invalide()
			return supprimerUnEntrainementParDefaut()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return supprimerUnEntrainementParDefaut()


#===========================================================================================#
#= 	Supprimer un objet entrainement                                 (Sur appel)        =#
#===========================================================================================#

def supprimerObjetEntrainement(entrainement_id):
	for entrainement in liste_entrainements:
		if entrainement.getIdentifiantEntrainement() == entrainement_id:
			print("\n\n"+ entrainement.__str__() + " supprimé\n")
			print("Effacement des données\n\n")
			del entrainement		


#===========================================================================================#
#= 	Supprimer un entrainement de la base de donnée                  (Sur appel)        =#
#===========================================================================================#

def supprimer_entrainement_BD(entrainement_id):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' DELETE FROM Entrainements_Table WHERE entrainement_id  = (?)''',(entrainement_id,))
	conn.commit()
	conn.close()
	afficherEntrainementsDansTerminal(liste_entrainements)


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#- 	Supprimer un entraînement dans un rapport existant              (Sur appel)        -*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def supprimer_Entrainement_Rapport(entrainement,entrainement_id):
	if len(liste_rapports) !=0:
		patient_id = entrainement.getPatientEntrainement().getIdentifiant()
		rapport_id = patient_id
		for rapport in liste_rapports:
			if rapport_id == rapport.getIdentifiantRapport():

				modifierEntrainementRapport(entrainement_id,patient_id, None)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER UN ENTRAINEMENT                                        (Choix 5-3-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modification_entrainement')
def modifierEntrainementParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER UN ENTRAINEMENT")
	print ("================================================================================\n")
	return render_template('530_modifier_entrainement.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER LE PATIENT D'UN ENTRAINEMENT                           (Choix 5-3-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_entrainement_patient')
def modifierEntrainementPatientParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER LE PATIENT D'UN ENTRAINEMENT")
	print ("================================================================================\n")
	entrainements_liste_X = formaterEntrainementHTML1()
	afficherEntrainementsDansTerminal(liste_entrainements)
	return render_template('531_modifier_entrainement_patient.html',entrainements=entrainements_liste_X)


@app.route('/modifier_entrainement_patient', methods=['POST'])
def modifierEntrainementPatientX():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER LE PATIENT D'UN ENTRAINEMENT\n")
	try:
		entrainement_id 		= request.form["entrainement_id"]
		entrainement_id                 = int(entrainement_id)
		entrainement                    = objectiser_entrainement(entrainement_id)
		identifiant                     = request.form["patient_id"]
		identifiant                     = int(identifiant)
		if testerExistenceEntrainement(entrainement_id) == True and testerExistencePatient(identifiant) == True :
			entrainement=objectiser_entrainement(entrainement_id)
			vieux_identifiant = entrainement.getPatientEntrainement().getIdentifiant()
			afficherEntrainementsAvecID(entrainement_id)
			entrainements_liste_Y=formaterEntrainementHTML2(entrainement_id)	
			patient                         = objectiser_patient(identifiant)
			entrainement                    = objectiser_entrainement(entrainement_id)
			nouveau_nom                     = patient.getNom()
			nouveau_prenom                  = patient.getPrenom()
			modifierEntrainementPatient(entrainement,patient,identifiant,nouveau_nom,nouveau_prenom)
			rafraichirEntrainement()
			afficherEntrainementsAvecID(entrainement_id)
			entrainements_liste_Z=formaterEntrainementHTML2(entrainement_id)
			actualiserEntrainementRapport2(vieux_identifiant)
			actualiserEntrainementRapport2(identifiant)
			return render_template('531_modifier_entrainement_patient.html',entrainement=entrainements_liste_Y, entrainementz=entrainements_liste_Z)
		else:
			entree_invalide
			return modifierEntrainementPatientParDefaut()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierEntrainementPatientParDefaut()




#===========================================================================================#
#= 	Modifier le patient d'un entrainement                                              =#
#===========================================================================================#

def modifierEntrainementPatient(entrainement,patient,identifiant,nouveau_nom,nouveau_prenom):
	entrainement_id=entrainement.getIdentifiantEntrainement()
	entrainement.setPatientEntrainement(patient)
	entrainement.setNomEntrainement(nouveau_nom)
	entrainement.setPrenomEntrainement(nouveau_prenom)
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' UPDATE Entrainements_Table SET patient_id = (?) WHERE entrainement_id = (?)''',(identifiant,entrainement_id))
	cursor.execute(''' UPDATE Entrainements_Table SET nom = (?) WHERE entrainement_id = (?)''',(nouveau_nom,entrainement_id))
	cursor.execute(''' UPDATE Entrainements_Table SET prenom = (?) WHERE entrainement_id = (?)''',(nouveau_prenom,entrainement_id))
	conn.commit()
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER L'ENTRAINEUR D'UN ENTRAINEMENT                         (Choix 5-3-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modifier_entrainement_entraineur')
def modifierEntrainementEntraineurParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER L'ENTRAINEUR D'UN ENTRAINEMENT")
	print ("================================================================================\n")
	entrainements_liste_X = formaterEntrainementHTML1()
	afficherEntrainementsDansTerminal(liste_entrainements)
	return render_template('532_modifier_entrainement_entraineur.html',entrainements=entrainements_liste_X)


@app.route('/modifier_entrainement_entraineur', methods=['POST'])
def modifierEntrainementEntraineurX():
	print ("\n\n--------------------------------------------------------------------------------\nMODIFIER L'ENTRAINEUR D'UN ENTRAINEMENT\n")
	try:
		entrainement_id 		= request.form["entrainement_id"]
		entrainement_id                 = int(entrainement_id)
		entrainement                    = objectiser_entrainement(entrainement_id)
		identifiant                     = request.form["entraineur_id"]
		identifiant                     = int(identifiant)
		if testerExistenceEntrainement(entrainement_id) == True and testerExistenceEntraineur(identifiant) == True :
			afficherEntrainementsAvecID(entrainement_id)
			entrainements_liste_Y=formaterEntrainementHTML2(entrainement_id)	
			entraineur                      = objectiser_entraineur(identifiant)
			entrainement                    = objectiser_entrainement(entrainement_id)
			nouveau_nom                     = entraineur.getNom()
			nouveau_prenom                  = entraineur.getPrenom()
			modifierEntrainementEntraineur(entrainement,entraineur,identifiant)
			rafraichirEntrainement()
			afficherEntrainementsAvecID(entrainement_id)
			entrainements_liste_Z=formaterEntrainementHTML2(entrainement_id)	
			actualiserEntrainementRapport(entrainement_id)
			return render_template('532_modifier_entrainement_entraineur.html',entrainement=entrainements_liste_Y, entrainementz=entrainements_liste_Z)
		else:
			entree_invalide
			return modifierEntrainementEntraineurParDefaut()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return modifierEntrainementEntraineurParDefaut()


#===========================================================================================#
#= 	Modifier l'entraineur d'un entrainement                                            =#
#===========================================================================================#

def modifierEntrainementEntraineur(entrainement,entraineur,identifiant):
	entrainement_id=entrainement.getIdentifiantEntrainement()
	entrainement.setEntraineurEntrainement(entraineur)
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' UPDATE Entrainements_Table SET entraineur_id = (?) WHERE entrainement_id = (?)''',(identifiant,entrainement_id))
	conn.commit()
	conn.close()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION ENTRAINEMENT                                        (Choix 5-4-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/information_entrainement')
def informationEntrainementParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION ENTRAINEMENT")
	print ("================================================================================\n")
	return render_template('540_info_entrainement.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES ENTRAINEMENTS PAR ORDRE ALPHABÉTIQUE DES PATIENTS     (Choix 5-4-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_entrainements_liste_np')
def afficherListeEntrainementNPParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES ENTRAINEMENTS PAR ORDRE ALPHABÉTIQUE DES PATIENTS")
	print ("================================================================================\n")
	entrainements_liste_X=afficherEntrainementListeNP()
	return render_template('541_afficher_entrainements_liste_np.html', entrainements=entrainements_liste_X)


def afficherEntrainementListeNP():
	print ("\n\n--------------------------------------------------------------------------------\nLISTE DES ENTRAINEMENTS PAR ORDRE ALPHABÉTIQUE DES PATIENTS")
	entrainements_liste_X=[]
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	entrainements_liste_BD=cursor.execute(''' SELECT * FROM Entrainements_Table ORDER BY nom ASC,prenom ASC''')
	print ("\n\n"+'{:<15}{:<20}{:<28}{:<16}'.format(" ENTRAINEMENT"," PLAGE HORAIRE"," PATIENT"," ENTRAINEUR")+'{:<15}{:<20}{:<28}{:<16}'.format("  ID","  Date & Heure","  ID,  Nom & Prénom","  Nom & Prénom")+"\n")
	for attributs in entrainements_liste_BD:
		entrainement_id         = attributs[0]
		date                    = attributs[1]
		heure                   = str(attributs[2])+":00"
		patient_id              = attributs[3]
		entraineur_id           = attributs[4]
		patient                 = objectiser_patient(patient_id)
		entraineur              = objectiser_entraineur(entraineur_id)
		if patient != None and entraineur != None:
			compte          = compterEntrainementHTML(entrainement_id)
			nom_prenom      = str(patient.getNom())+" "+str(patient.getPrenom())
			nom_prenom2     = str(entraineur.getNom())+" "+str(entraineur.getPrenom())
			entrainementx=(entrainement_id,date,heure,patient.getNom(),patient.getPrenom(),patient_id,entraineur.getNom(),entraineur.getPrenom(),entraineur.getIdentifiant(),compte)
			entrainements_liste_X.append(entrainementx)
			print (' {:<15}{:<12}{:<8}{:<4}{:<24}{:<15}'.format(entrainement_id,date,heure,patient_id,nom_prenom,nom_prenom2))
	print("\n\n\n")
	conn.close()		
	return entrainements_liste_X



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES ENTRAINEMENTS PAR ORDRE CHRONOLOGIQUE                 (Choix 5-4-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_entrainements_liste_id')
def afficherListeEntrainementsIDParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES ENTRAINEMENTS PAR ORDRE CHRONOLOGIQUE")
	print ("================================================================================\n")
	afficherLesEntrainements()
	entrainements_liste_X=formaterEntrainementHTML1()
	return render_template('542_afficher_entrainements_liste_id.html', entrainements=entrainements_liste_X)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AFFICHER UN ENTRAINEMENT À L'AIDE D'UN IDENTIFIANT ENTRAINEMENT (Choix 5-4-3)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_entrainement_id')
def afficherUnEntrainementIDParDefaut():
	print ("\n\n================================================================================")
	print("AFFICHER UN ENTRAINEMENT À L'AIDE D'UN IDENTIFIANT ENTRAINEMENT")
	print ("================================================================================\n")
	return render_template('543_afficher_un_entrainement_id.html')

@app.route('/afficher_un_entrainement_id', methods=['POST'])
def afficherUnEntrainementIDX():
	print ("\n\n--------------------------------------------------------------------------------\nAFFICHER UN ENTRAINEMENT À L'AIDE D'UN IDENTIFIANT ENTRAINEMENT\n")
	try:
		entrainement_id                 = request.form["entrainement_id"]
		entrainement_id                 = int(entrainement_id)
		entrainementx                  = objectiser_entrainement(entrainement_id)
		if entrainementx != None and entrainement_id != "":
			for entrainement in liste_entrainements:
				if entrainement.getIdentifiantEntrainement()==entrainement_id:
					afficherEntrainementsAvecID(entrainement_id)	
					entrainements_liste_Y = formaterEntrainementHTML2(entrainement_id)	
			return render_template('543_afficher_un_entrainement_id.html', entrainements=entrainements_liste_Y)
		else:
			entree_invalide()
			return afficherUnEntrainementIDParDefaut()
	except ValueError:
		entree_invalide()
		return afficherUnEntrainementIDParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AFFICHER LES ENTRAINEMENTS D'UN PATIENT                         (Choix 5-4-4)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_entrainement_np')
def afficherUnEntrainementNPParDefaut():
	print ("\n\n================================================================================")
	print("AFFICHER LES ENTRAINEMENTS D'UN PATIENT")
	print ("================================================================================\n")
	return render_template('544_afficher_un_entrainement_np.html')


@app.route('/afficher_un_entrainement_np', methods=['POST'])
def afficherUnEntrainementNPX():
	print ("\n\n--------------------------------------------------------------------------------\nAFFICHER LES ENTRAINEMENTS D'UN PATIENT\n")
	try: 
		nom = request.form["nom"]
		prenom  = request.form["prenom"]
		identifiant = request.form["identifiant"]
		if identifiant != "":
			identifiant = int(identifiant)
			patient=objectiser_patient(identifiant)
			if patient != None:
				entrainements_liste_Y=classerEntrainementsAvecIDPatient(patient)
				return render_template('544_afficher_un_entrainement_np.html',entrainements=entrainements_liste_Y)
			else:
				return afficherUnEntrainementNPParDefaut()
		elif nom != "" or prenom != "":
			entrainements_liste_X=classerEntrainementsAvecNPPatient(nom,prenom)
			return render_template('544_afficher_un_entrainement_np.html',entrainements=entrainements_liste_X)
		else:
			return afficherUnEntrainementNPParDefaut()
	except ValueError:
		return afficherUnEntrainementNPParDefaut()


def classerEntrainementsAvecIDPatient(patient):
	entrainements_liste_X=[]
	for entrainement in liste_entrainements:
		if entrainement.getPatientEntrainement() == patient:
			afficherEntrainementsAvecID(entrainement.getIdentifiantEntrainement())
			entrainementy=formaterEntrainementHTML(entrainement.getIdentifiantEntrainement())
			entrainements_liste_X.append(entrainementy)
	return entrainements_liste_X


def classerEntrainementsAvecNPPatient(nom,prenom):
	entrainements_liste_Z=[]
	entrainements_liste_X=[]
	for patient in liste_patients :
		if len(liste_patients)  == 0 :
			print("\nAucun patient\n")
		elif patient.getNom().lower() == nom.lower() and patient.getPrenom().lower() == prenom.lower():	
			for entrainement in liste_entrainements:
				if entrainement.getPatientEntrainement() == patient :
					entrainements_liste_Z.append(entrainement)
					afficherEntrainementsAvecID(entrainement.getIdentifiantEntrainement())


		if nom == "" or prenom == "":
			if patient.getNom().lower() == nom.lower() or patient.getPrenom().lower() == prenom.lower():	
				for entrainement in liste_entrainements:
					if entrainement.getPatientEntrainement() == patient :
						entrainements_liste_Z.append(entrainement)	
						afficherEntrainementsAvecID(entrainement.getIdentifiantEntrainement())
	for entrainement in entrainements_liste_Z:
		entrainement_id = entrainement.getIdentifiantEntrainement()
		entrainementx=formaterEntrainementHTML(entrainement_id)		
		entrainements_liste_X.append(entrainementx)
	return entrainements_liste_X



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@#=                                                                                       =#@
#@#=                                 FONCTIONS RAPPORT                                     =#@
#@#=                                                                                       =#@
#@#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#===========================================================================================#
# 	Mise à jour des rapports                                        (Sur appel)        =#
#===========================================================================================#

def rafraichirRapport():
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute(''' SELECT * FROM Rapports_Table ORDER BY nom ASC,prenom ASC ''')
	lignesTableRapport=cursor.fetchall()
	del liste_rapports [:]
	for attribut in lignesTableRapport:
		rapport_id              = attribut[0]
		patient_id              = attribut[1]
		entrainements_requis    = attribut[2]
		entrainements_prevus    = attribut[3]
		programmes_ids          = attribut[4]
		entrainements_ids       = attribut[5]
		nom                     = attribut[6]
		prenom                  = attribut[7]
		rafraichirRapport2(rapport_id,patient_id,entrainements_requis,entrainements_prevus,programmes_ids,entrainements_ids,nom,prenom)
	conn.close()


# Mini script de récupération des attributs et objets de la classe Rapport
def rafraichirRapport2(rapport_id,patient_id,entrainements_requis,entrainements_prevus,programmes_ids,entrainements_ids,nom,prenom):
	patient                         = objectiser_patient(rapport_id)
	programmes_patient              = rafraichirProgrammeRapport(programmes_ids)
	entrainements_patient           = rafraichirEntrainementRapport(entrainements_ids)
	rapport                         = Rapport(rapport_id,patient,entrainements_requis,entrainements_prevus,programmes_patient,entrainements_patient,nom,prenom)
	liste_rapports.append(rapport)


# Récupération d'une liste de programmes à partir d'ID sous forme de caractères (BD)
def rafraichirProgrammeRapport(programmes_ids):
	programmes_patient_temp=programmes_ids.split("-")
	programmes_patient = []
	for prog_id in programmes_patient_temp:
		if prog_id != None:
			for programme in liste_programmes:
				if str(programme.getIdentifiantProgramme()) == prog_id:
					programmes_patient.append(programme)
	return programmes_patient


# Récupération d'une liste d'entrainements à partir d'ID sous forme de caractères (BD)
def rafraichirEntrainementRapport(entrainements_ids):
	entrainements_patient_temp=entrainements_ids.split("-")
	entrainements_patient=[]
	for ent_id in entrainements_patient_temp:
		if ent_id != None:
			for entrainement in liste_entrainements:
				if str(entrainement.getIdentifiantEntrainement())==ent_id:
					entrainements_patient.append(entrainement)
	return entrainements_patient


#===========================================================================================#
#= 	Vérifier si un rapport existe                                                      =#
#===========================================================================================#

def testerExistenceRapport(rapport_id):
	existence = []
	for rapport in liste_rapports:
		if rapport_id == rapport.getIdentifiantRapport():
			existence.append(rapport.getIdentifiantRapport())
	if len(existence)!=0:
		return True
	else:
		return False


#===========================================================================================#
#= 	Formater rapports pour HTML                                                        =#
#===========================================================================================#

def formaterRapportHTML(rapport_id):
	patient                 = objectiser_patient(rapport_id)
	rapport                 = objectiser_rapport(rapport_id)
	patient_nom             = rapport.getPatientRapport().getNom().upper()
	patient_prenom          = rapport.getPatientRapport().getPrenom().upper()
	entrainements_requis    = rapport.getEntRequisRapport()
	entrainements_prevus	= rapport.getEntPrevusRapport()
	nbr_programmes          = progRapport(rapport)
	programmes_liste_Y      = classerProgrammesAvecIDPatient(patient)	
	entrainements_liste_Y   = classerEntrainementsAvecIDPatient(patient)
	rapportx                = (rapport_id,patient_nom,patient_prenom,entrainements_requis,entrainements_prevus,nbr_programmes)
	print (rapportx)
	return rapportx,programmes_liste_Y,entrainements_liste_Y


def formaterRapportHTML1():
	rapports_liste_X = []
	for rapport in liste_rapports:
		rapport_id = rapport.getIdentifiantRapport()
		rapportx,programmes_liste_Y,entrainements_liste_Y=formaterRapportHTML(rapport_id)		
		rapports_liste_X.append(rapportx)		
	return rapports_liste_X


def formaterRapportHTML2(rapport_id):
	rapports_liste_Y =[]
	rapport=objectiser_rapport(rapport_id)
	rapporty,programmes_liste_Y,entrainements_liste_Y=formaterRapportHTML(rapport_id)
	rapports_liste_Y.append(rapporty)
	return rapports_liste_Y,programmes_liste_Y,entrainements_liste_Y

def formaterRapportChronoHTML():
	rapports_liste_X = []
	for rapport in liste_rapports:
		rapport_id = rapport.getIdentifiantRapport()
		rapportx,programmes_liste_Y,entrainements_liste_Y=formaterRapportHTML(rapport_id)		
		rapports_liste_X.append(rapportx)
	rapports_liste_X.sort()		
	return rapports_liste_X

def progRapport(rapport):
	nbr_programmes=0
	programmes=rapport.getProgrammesRapport()
	for programme in programmes:
		nbr_programmes += 1
	return nbr_programmes
				


#===========================================================================================#
#= 	Terminal : Afficher un rapport à l'aide d'un identifiant          (Sur Appel)      =#
#===========================================================================================#

def afficherRapportsAvecID(rapport_id):
	for rapport in liste_rapports:
		if rapport != None:
			if rapport.getIdentifiantRapport() == rapport_id:
				print (rapport)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MENU RAPPORT                                                    (Choix 6-0-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@app.route('/menu_rapport')
def menuRapportParDefaut():
	print ("\n\n================================================================================")
	print("MENU RAPPORT")
	print ("================================================================================\n")
	return render_template('600_menu_rapport.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AJOUTER UN RAPPORT                                              (Choix 6-1-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/ajouter_rapport')
def ajouterRapportParDefaut():
	print ("\n\n================================================================================")
	print("AJOUTER UN RAPPORT")
	print ("================================================================================\n")
	rapports_liste_X = formaterRapportHTML1()
	return render_template('610_ajouter_rapport.html', rapports=rapports_liste_X)


@app.route ('/ajouter_rapport', methods=['POST'])
def ajouterRapport():
	print ("\n\n--------------------------------------------------------------------------------\nAJOUTER UN RAPPORT")
	try:
		identifiant = request.form["patient_id"]
		if identifiant != "":
			identifiant = int(identifiant)
			if testerExistencePatient(identifiant) == True:
				if testerExistenceRapport(identifiant) == False:
					rapport_id              = identifiant
					patient                 = objectiser_patient(identifiant)
					nom                     = patient.getNom()
					prenom                  = patient.getPrenom()
					ajouterObjetRapport(rapport_id,patient,nom,prenom)
					rapports_liste_Y,programmes_liste_Y,entrainements_liste_Y = formaterRapportHTML2(patient.getIdentifiant())
					rapport                 = objectiser_rapport(identifiant)
					return render_template('610_ajouter_rapport.html',rapport = rapports_liste_Y, programmes = programmes_liste_Y ,entrainements = entrainements_liste_Y)
				else:
					invalide = ["Ce rapport existe déjà"]
					message_erreur = messageErreur(invalide)
					return render_template('610_ajouter_rapport.html',message_erreur=message_erreur)
			else:
				invalide = ["Ce patient n'existe pas"]
				message_erreur = messageErreur(invalide)
				return render_template('610_ajouter_rapport.html',message_erreur=message_erreur)
		else:
			entree_invalide()
			return ajouterRapportParDefaut()
	except ValueError:
		entree_invalide()
		return ajouterRapportParDefaut()


#===========================================================================================#
#  	Création d'un objet rapport de la classe Rapport                (Sur Appel)        =#
#===========================================================================================#
def ajouterObjetRapport(rapport_id,patient,nom,prenom):
	(patient,entrainements_requis,entrainements_prevus,programmes_patient,entrainements_patient)=rapportPatientBref(rapport_id)
	rapport=Rapport(rapport_id,patient,entrainements_requis,entrainements_prevus,programmes_patient,entrainements_patient,nom,prenom)
	liste_rapports.append(rapport)
	conversionBDRapport(rapport,programmes_patient,entrainements_patient,nom,prenom)


#===========================================================================================#
#  	Convertir programmes et entrainements en caractères pour la BD  (Sur Appel)        =#
#===========================================================================================#
def conversionBDRapport(rapport,programmes_patient,entrainements_patient,nom,prenom):
	programmes_ids=""
	entrainements_ids=""
	for programme in programmes_patient:
		prog_id=str(programme.getIdentifiantProgramme())
		programmes_ids +="".join(prog_id+"-")
	for entrainement in entrainements_patient:
		ent_id=str(entrainement.getIdentifiantEntrainement())
		entrainements_ids +="".join(ent_id+"-")
	ajout_Rapport_BD(rapport,programmes_ids,entrainements_ids,nom,prenom)


#===========================================================================================#
# 	Enregistrer un nouveau rapport dans la base de données          (Sur Appel)        =#
#===========================================================================================#
def ajout_Rapport_BD(rapport,programmes_ids,entrainements_ids,nom,prenom):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()
	cursor.execute('''INSERT INTO Rapports_Table(rapport_id,patient_id,entrainements_requis,entrainements_prevus,programmes_patient,entrainements_patient,nom,prenom) VALUES(?,?,?,?,?,?,?,?)''',(rapport.getIdentifiantRapport(),rapport.getPatientRapport().getIdentifiant(),rapport.getEntRequisRapport(),rapport.getEntPrevusRapport(),programmes_ids,entrainements_ids,nom,prenom))
	conn.commit()
	conn.close()
	rafraichirRapport()
	afficherRapportsAvecID(rapport.getIdentifiantRapport())
	print("\n\n\t\t\t    RAPPORT #"+str(rapport.getIdentifiantRapport()) + " AJOUTÉ\n")
	print("\n\n")



#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#
#-*                                CONSOLIDER LES DONNÉES                                 *-#
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Rapport Patient Bref : Organiser les données                    (Sur appel)        -*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def rapportPatientBref(identifiant):
	rafraichirListes()
	patient                         = objectiser_patient(identifiant)
	priorite_programme              = classerProgrammesPatient(patient)       # Données A
	programmes_patient,entrainements_requis = classerProgrammesPatient2(priorite_programme)
	entrainements_prevus            = compterEntrainementsPatient(patient)    # Données B
	avisSeancesExcedentaires(patient,entrainements_requis)                    # Données C			
	identifiant_entrainement        = classerEntrainementsPatient(patient)    # Données D
	entrainements_patient           = classerEntrainementsPatient2(identifiant_entrainement)		
	return(patient,entrainements_requis,entrainements_prevus,programmes_patient,entrainements_patient)


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Classer les Programmes et compter les séances requises          (Données A)        -* 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def classerProgrammesPatient(patient):
	priorite_programme = []
	for programme in liste_programmes:
		if patient == programme.getPatientProgramme():
			priorite_ID = (str(programme.getPrioriteCliniqueProgramme()),str(programme.getIdentifiantProgramme()))
			priorite_programme.append(priorite_ID)
	priorite_programme = sorted (priorite_programme)
	return priorite_programme

def classerProgrammesPatient2(priorite_programme):
	programmes_patient = []
	entrainement_requis = 0
	for p_p in priorite_programme:
		programme_id = p_p[1]
		for programme in liste_programmes:
			if str(programme_id) == str(programme.getIdentifiantProgramme()):
				programmes_patient.append(programme)
				entrainement_requis += programme.getNombreSeancesProgramme()
	return (programmes_patient,entrainement_requis)


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Compter les entrainements d'un patient                          (Données B)        -* 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def compterEntrainementsPatient(patient):
	entrainements_prevus = compterEntrainementsPatient2(patient)
	return entrainements_prevus

def compterEntrainementsPatient2(patient):
	ep = []
	entrainement_prevus=0
	for entrainement in liste_entrainements:
		if entrainement.getPatientEntrainement()==patient:
			entrainement_prevus += 1
			ep.append(entrainement_prevus)
	try:
		return max(ep)
	except ValueError:
		return 0


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Avis de séances excédentaires p/r au besoin des programmes      (Données C)        -* 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def avisSeancesExcedentaires(patient,entrainement_requis):
	entrainement_prevus = compterEntrainementsPatient(patient)
	if entrainement_prevus > entrainement_requis:
		print("\n\n*** AVIS IMPORTANT ***\nLe nombre d'entraînements prévus est supérieur au besoin des programmes")
		print("Supprimer des entraînements, ajouter un programme ou modifier celui en cours\n\n")


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Classer les entrainements d'un patient                          (Données D)        -* 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def classerEntrainementsPatient(patient):
	identifiant_entrainement = []
	for entrainement in liste_entrainements:
		if entrainement.getPatientEntrainement() == patient:
			i_e = (entrainement.getIdentifiantEntrainement(),entrainement)
			identifiant_entrainement.append(i_e)
	identifiant_entrainement = sorted (identifiant_entrainement)
	return identifiant_entrainement

def classerEntrainementsPatient2(identifiant_entrainement):
	entrainements_patient = []
	entrainements_compte = 0
	for i_e in identifiant_entrainement:
		entrainements_compte += 1
		entrainement = i_e[1]
		compte_entrainement = (entrainements_compte,entrainement)
		entrainements_patient.append(entrainement)
		entraineur = entrainement.getEntraineurEntrainement().getPrenom()+" "+entrainement.getEntraineurEntrainement().getNom()
	return entrainements_patient



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	SUPPRIMER UN RAPPORT                                            (Choix 6-2-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/supprimer_rapport')
def supprimerUnRapportParDefaut():
	print ("\n\n================================================================================")
	print("SUPPRIMER UN RAPPORT")
	print ("================================================================================\n")

#	afficherRapportsDansTerminal(liste_rapports)
	rapports_liste_X=formaterRapportHTML1()
	return render_template('620_supprimer_rapport.html', rapports=rapports_liste_X)


@app.route ('/supprimer_rapport', methods=['POST'])
def supprimerRapportX():
	print ("\n\n--------------------------------------------------------------------------------\nSUPPRIMER UN RAPPORT")
	try:
		rapport_id = request.form["rapport_id"]
		if rapport_id != "":
			rapport_id = int(rapport_id)
			if testerExistenceRapport(rapport_id) == True:
				rapports_liste_Y,programmes_liste_Y,entrainements_liste_Y = formaterRapportHTML2(rapport_id)
	#			afficherRapportsAvecID(rapport_id)	
				supprimerObjetRapport(rapport_id)			
				supprimer_rapport_BD(rapport_id)
				return render_template('620_supprimer_rapport.html', rapport = rapports_liste_Y, programmes = programmes_liste_Y ,entrainements = entrainements_liste_Y)
			else:
				entree_invalide()
				return supprimerUnRapportParDefaut()
		else:
			entree_invalide()
			return supprimerUnRapportParDefaut()
		rafraichirListes()
	except ValueError:
		entree_invalide()
		return supprimerUnRapportParDefaut()


#===========================================================================================#
#= 	Supprimer un objet rapport                                      (Sur appel)        =#
#===========================================================================================#

def supprimerObjetRapport(rapport_id):
	for rapport in liste_rapports:
		if rapport.getIdentifiantRapport() == rapport_id:
			afficherRapportsAvecID(rapport.getIdentifiantRapport())
			print("\n\n\t\t\t    RAPPORT #"+str(rapport.getIdentifiantRapport()) + " SUPPRIMÉ\n")
			print("\n\n")
			del rapport		


#===========================================================================================#
#= 	Supprimer un rapport de la base de donnée                       (Sur appel)        =#
#===========================================================================================#

def supprimer_rapport_BD(rapport_id):
	conn   = sqlite3.connect('NFB.db')
	cursor = conn.cursor()	
	cursor.execute(''' DELETE FROM Rapports_Table WHERE rapport_id  = (?)''',(rapport_id,))
	conn.commit()
	conn.close()
	rafraichirListes()
#	afficherRapportsDansTerminal(liste_entrainements)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	MODIFIER UN RAPPORT                                             (Choix 6-3-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/modification_rapport')
def modifierrapportParDefaut():
	print ("\n\n================================================================================")
	print("MODIFIER UN RAPPORT")
	print ("================================================================================\n")
	return render_template('630_modifier_rapport.html')


#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-	Mise à jour des rapports suite à une modification entraînement  (Sur Appel)        -* 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def actualiserEntrainementRapport(entrainement_id):
	for entrainement in liste_entrainements:
		if str(entrainement.getIdentifiantEntrainement()) == str(entrainement_id):
			patient = entrainement.getPatientEntrainement()
			identifiant = patient.getIdentifiant()
			if len(liste_rapports) != 0:
				actualiserEntrainementRapport2(identifiant)

def actualiserEntrainementRapport2(identifiant):
	for rapport in liste_rapports:
		if identifiant == rapport.getIdentifiantRapport():
			patient= objectiser_patient(identifiant)
			conn   = sqlite3.connect('NFB.db')
			cursor = conn.cursor()
			cursor.execute('''DELETE FROM Rapports_Table WHERE rapport_id = (?)''',(identifiant,))
			conn.commit()
			conn.close()
			rafraichirListes()
			supprimerObjetRapport(identifiant)
			ajouterObjetRapport(identifiant,patient,patient.getNom(),patient.getPrenom())
			rafraichirListes()
			print(rapport)

#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#- 	MODIFIER UN ENTRAINEMENT DE LA CLASSE RAPPORT VIA ENTRAINEMENT  (Sur appel)        -*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

def modifierEntrainementRapport(entrainement_id,rapport_id,patient_id2):
	entrainements_patient2=modifierEntrainementRapport2(entrainement_id,rapport_id)
	rapport=objectiser_rapport(rapport_id)
	if rapport != None:
		patient=rapport.getPatientRapport()
		rapport.setEntrainementsRapport(entrainements_patient2)
		conn   = sqlite3.connect('NFB.db')
		cursor = conn.cursor()
		cursor.execute('''DELETE FROM Rapports_Table WHERE rapport_id = (?)''',(rapport_id,))
		conn.commit()
		conn.close()
		supprimerObjetRapport(rapport_id)
		ajouterObjetRapport(rapport_id,patient,patient.getNom(),patient.getPrenom())
	modifierEntrainementRapport3(entrainement_id,patient_id2)


def modifierEntrainementRapport2(entrainement_id,rapport_id):
	entrainements_patient2=[]
	entrainements_prevus = 0
	for rapport in liste_rapports:
		if rapport.getIdentifiantRapport()==rapport_id:
			entrainements_prevus += rapport.getEntPrevusRapport()
			entrainements_patient=rapport.getEntrainementsRapport()
			for entrainement in entrainements_patient:
				if entrainement_id != entrainement.getIdentifiantEntrainement():
					entrainements_patient2.append(entrainement)
					entrainements_prevus -= 1
	return entrainements_patient2,entrainements_prevus


def modifierEntrainementRapport3(entrainement_id,patient_id2):
	rapport_id=patient_id2
	entrainements_patient2=modifierEntrainementRapport2(entrainement_id,rapport_id)
	rapport=objectiser_rapport(rapport_id)
	if rapport != None:
		patient=rapport.getPatientRapport()
		rapport.setEntrainementsRapport(entrainements_patient2)
		conn   = sqlite3.connect('NFB.db')
		cursor = conn.cursor()
		cursor.execute('''DELETE FROM Rapports_Table WHERE rapport_id = (?)''',(rapport_id,))
		conn.commit()
		conn.close()
		supprimerObjetRapport(rapport_id)
		ajouterObjetRapport(rapport_id,patient,patient.getNom(),patient.getPrenom())
		rafraichirListes()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	INFORMATION RAPPORT                                             (Choix 6-4-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/information_rapport')
def informationRapportParDefaut():
	print ("\n\n================================================================================")
	print("INFORMATION RAPPORT")
	print ("================================================================================\n")
	return render_template('640_info_rapport.html')



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTES DES RAPPORTS PAR ORDRE ALPHABÉTIQUE DES PATIENTS         (Choix 6-4-1)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_rapports_liste_np')
def afficherListeRapportNPParDefaut():
	print ("\n\n================================================================================")
	print("LISTES DES RAPPORTS PAR ORDRE ALPHABÉTIQUE DES PATIENTS")
	print ("================================================================================\n")
	afficherLesRapports()
	print("\n\n")	
	rapports_liste_X = formaterRapportHTML1()
	return render_template('641_afficher_rapports_liste_np.html', rapports=rapports_liste_X)

@app.route('/afficher_rapports_liste_np', methods=['POST'])
def afficherListeRapportNPX():
	print ("\n\n--------------------------------------------------------------------------------\nLISTES DES RAPPORTS PAR ORDRE ALPHABÉTIQUE DES PATIENTS\n")
	try:
		rapport_id                     = request.form["rapport_id"]
		rapport_id                     = int(rapport_id)
		if testerExistenceRapport(rapport_id) == True:
			rapportx                       = objectiser_rapport(rapport_id)
			for rapport in liste_rapports:
				if rapport.getIdentifiantRapport()==rapport_id:
					afficherRapportsAvecID(rapport_id)
					print("\n\n")	
					rapports_liste_Y,programmes_liste_Y,entrainements_liste_Y = formaterRapportHTML2(rapport_id)	
			return render_template('641_afficher_rapports_liste_np.html', rapport = rapports_liste_Y, programmes = programmes_liste_Y ,entrainements = entrainements_liste_Y)
		else:
			entree_invalide()
			return afficherListeRapportNPParDefaut()
	except ValueError:
		entree_invalide()
		return afficherListeRapportNPParDefaut()



#===========================================================================================#
#=  	Afficher tous les rapports de la classe Rapport                  (Sur appel)       =#
#===========================================================================================#
def afficherLesRapports():
	for rapport in liste_rapports:
		print(rapport)



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	LISTE DES ENTRAINEMENTS PAR ORDRE CHRONOLOGIQUE DES PATIENTS    (Choix 6-4-2)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_rapports_liste_id')
def afficherListerapportIDParDefaut():
	print ("\n\n================================================================================")
	print("LISTE DES RAPPORTS PAR ORDRE CHRONOLOGIQUE DES PATIENTS")
	print ("================================================================================\n")
	rapports_liste_X=formaterRapportChronoHTML()
	return render_template('642_afficher_rapports_liste_id.html', rapports=rapports_liste_X)


@app.route('/afficher_rapports_liste_id', methods=['POST'])
def afficherListerapportIDX():
	print ("\n\n--------------------------------------------------------------------------------\nLISTE DES RAPPORTS PAR ORDRE CHRONOLOGIQUE DES PATIENTS\n")
	try:
		rapport_id                     = request.form["rapport_id"]
		rapport_id                     = int(rapport_id)
		if testerExistenceRapport(rapport_id) == True:
			rapportx                       = objectiser_rapport(rapport_id)
			for rapport in liste_rapports:
				if rapport.getIdentifiantRapport()==rapport_id:
					afficherRapportsAvecID(rapport_id)
					print("\n\n")	
					rapports_liste_Y,programmes_liste_Y,entrainements_liste_Y = formaterRapportHTML2(rapport_id)	
			return render_template('642_afficher_rapports_liste_id.html', rapport = rapports_liste_Y, programmes = programmes_liste_Y ,entrainements = entrainements_liste_Y)
		else:
			entree_invalide()
			return afficherListerapportIDParDefaut()
	except ValueError:
		entree_invalide()
		return afficherListerapportIDParDefaut()

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AFFICHER UN RAPPORT À L'AIDE D'UN ID RAPPORT / PATIENT          (Choix 6-4-3)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_rapport_id')
def afficherUnRapportIDParDefaut():
	print ("\n\n================================================================================")
	print("AFFICHER UN RAPPORT À L'AIDE D'UN ID RAPPORT / PATIENT")
	print ("================================================================================\n")
	afficherLesRapports()
	print("\n\n")	
	rapports_liste_X = formaterRapportHTML1()
	return render_template('643_afficher_un_rapport_id.html', rapports=rapports_liste_X)


@app.route('/afficher_un_rapport_id', methods=['POST'])
def afficherUnRapportIDX():
	print ("\n\n--------------------------------------------------------------------------------\nAFFICHER UN RAPPORT À L'AIDE D'UN ID RAPPORT / PATIENT\n")
	try:
		rapport_id                     = request.form["rapport_id"]
		rapport_id                     = int(rapport_id)
		if testerExistenceRapport(rapport_id) == True:
			rapportx                       = objectiser_rapport(rapport_id)
			for rapport in liste_rapports:
				if rapport.getIdentifiantRapport()==rapport_id:
					afficherRapportsAvecID(rapport_id)
					print("\n\n")	
					rapports_liste_Y,programmes_liste_Y,entrainements_liste_Y = formaterRapportHTML2(rapport_id)	
			return render_template('643_afficher_un_rapport_id.html', rapport = rapports_liste_Y, programmes = programmes_liste_Y ,entrainements = entrainements_liste_Y)
		else:
			entree_invalide()
			return afficherUnRapportIDParDefaut()
	except ValueError:
		entree_invalide()
		return afficherUnRapportIDParDefaut()



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AFFICHER LE RAPPORT D'UN PATIENT                                (Choix 6-4-4)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_un_rapport_np')
def afficherUnRapportNPParDefaut():
	print ("\n\n================================================================================")
	print("AFFICHER LE RAPPORT D'UN PATIENT")
	print ("================================================================================\n")
	rapports_liste_X = formaterRapportHTML1()
	return render_template('644_afficher_un_rapport_np.html', rapports=rapports_liste_X)


@app.route('/afficher_un_rapport_np', methods=['POST'])
def afficherUnRapportNPX():
	print ("\n\n--------------------------------------------------------------------------------\nAFFICHER LE RAPPORT D'UN PATIENT\n")
	try: 
		nom                     = request.form["nom"]
		prenom                  = request.form["prenom"]
		identifiant             = request.form["identifiant"]
		if identifiant != "":
			identifiant     = int(identifiant)
			rapport=objectiser_rapport(identifiant)
			if rapport != None:
				afficherRapportsAvecID(identifiant)
				print("\n\n")	
				rapports_liste_Y,programmes_liste_Y,entrainements_liste_Y = formaterRapportHTML2(identifiant)	
				return render_template('644_afficher_un_rapport_np.html',rapport = rapports_liste_Y, programmes = programmes_liste_Y ,entrainements = entrainements_liste_Y)
			else:
				return afficherUnRapportNPParDefaut()
		elif nom != "" or prenom != "":
			rapports_liste_X=classerRapportsAvecNPPatient(nom,prenom)
			return render_template('644_afficher_un_rapport_np.html',rapports=rapports_liste_X)
		else:
			return afficherUnRapportNPParDefaut()
	except ValueError:
		return afficherUnRapportNPParDefaut()


def classerRapportsAvecIDPatient(patient):
	rapports_liste_X=[]
	for rapport in liste_rapports:
		if rapport.getPatientRapport() == patient:
			afficherRapportsAvecID(rapport.getIdentifiantRapport())
			rapporty=formaterRapportHTML(rapport.getIdentifiantRapport())
			rapports_liste_X.append(rapporty)
	return rapports_liste_X


def classerRapportsAvecNPPatient(nom,prenom):
	rapports_liste_Z=[]
	rapports_liste_X=[]
	for patient in liste_patients :
		if len(liste_patients)  == 0 :
			print("\nAucun patient\n")
		if patient.getNom().lower() == nom.lower() and patient.getPrenom().lower() == prenom.lower():	
			for rapport in liste_rapports:
				if rapport.getPatientRapport() == patient :
					rapports_liste_Z.append(rapport)
					print(rapport)
		if nom == "" or prenom == "":
			if patient.getNom().lower() == nom.lower() or patient.getPrenom().lower() == prenom.lower():
				for rapport in liste_rapports:
					if rapport.getPatientRapport() == patient :	
						rapports_liste_Z.append(rapport)
						print(rapport)
	for rapport in rapports_liste_Z:
		rapport_id = rapport.getIdentifiantRapport()
		rapportx,programmes_liste_Y,entrainements_liste_Y=formaterRapportHTML(rapport_id)		
		rapports_liste_X.append(rapportx)		
	return rapports_liste_X



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@ 	AFFICHER LE RAPPORT D'UN PATIENT                                (Choix 6-5-0)      @@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@app.route('/afficher_suivis')
def afficherSuivisParDefaut():
	print ("\n\n================================================================================")
	print("SUIVI DES PATIENTS")
	print ("================================================================================\n")
	suivis_liste_X = []
	return render_template('650_afficher_suivis.html', suivis=suivis_liste_X)


@app.route('/afficher_suivis', methods=['POST'])
def afficherSuivisX():
	print ("\n\n--------------------------------------------------------------------------------\nSUIVI DES PATIENTS\n")
	try: 
		date		        = request.form["date"]
		if date != "" and len(date)==10:
			annee           = date[0]+date[1]+date[2]+date[3]
			mois            = date[5]+date[6]
			jour            = date[8]+date[9]
			date            = date[0]+date[1]+date[2]+date[3]+"-"+date[5]+date[6]+"-"+date[8]+date[9]
			moment1         = str(annee)+str(mois)+str(jour)+"00"
			moment2         = str(annee)+str(mois)+str(jour)+"23"			
			entrainement_du_jour = rapportsEntrainementsA(moment1,moment2)
			suivi_du_jour   = rapportsEntrainementsB(entrainement_du_jour)
			return render_template('650_afficher_suivis.html',suivi_du_jour=suivi_du_jour,date = date)
		else :
			return afficherSuivisParDefaut()
	except ValueError:
		return afficherSuivisParDefaut()


# Établir la liste de tous les entraînements pour une journée donnée
def rapportsEntrainementsA(moment1,moment2):
	entrainement_du_jour=[]
	for rapport in liste_rapports:
		entrainements_patient=rapport.getEntrainementsRapport()
		for entrainement in entrainements_patient:
			if str(entrainement.getIdentifiantEntrainement())>=moment1 and str(entrainement.getIdentifiantEntrainement())<=moment2:
				entrainement_id=str(entrainement.getIdentifiantEntrainement())
				id_ent_rap=(entrainement_id,entrainement,rapport)
				entrainement_du_jour.append(id_ent_rap)
	return entrainement_du_jour


# Établir la liste de tous les suivis des patients pour une journée donnée
def rapportsEntrainementsB(entrainement_du_jour):
	suivi_du_jour = []
	for id_ent_rap in sorted(entrainement_du_jour):
		entrainement_id=id_ent_rap[0]
		entrainement=id_ent_rap[1]
		rapport=id_ent_rap[2]
		rapport.suiviEntrainement(int(entrainement_id))
		entrainement_du_jour2 = suiviEntrainement_WEB(rapport,int(entrainement_id))
		suivi_du_jour.append(entrainement_du_jour2)
	return suivi_du_jour


# Mini-script de récupération et de transformation des données du rapport patient
def suiviEntrainement_WEB(rapport,entrainement_id):                                  
	comptes_entrainements           = rapport.suiviEntrainementRapport()
	compte,entrainement             = rapport.suiviEntrainementRapport2(entrainement_id,comptes_entrainements)
	seances_programmes              = rapport.suiviProgrammeRapport()
	(compte,str_compte,entrainement,seances_programmes,intervalles_programmes) = rapport.suiviEntrainementRapport3(compte,entrainement,seances_programmes)
	entrainement_du_jour2=suiviEntrainementRapport4_WEB(rapport,compte,str_compte,entrainement,seances_programmes,intervalles_programmes)
	return entrainement_du_jour2


# Script pour formater en HTML la progression d'un patient au travers de ses programmes
def suiviEntrainementRapport4_WEB(rapport,compte,str_compte,entrainement,seances_programmes,intervalles_programmes):
	entrainement_du_jour=[]
	for i in range (0,len(intervalles_programmes)):
		programme_str           = rapport.stringProgramme(i)
		programme               = rapport.retournerProgrammePourWeb(i)
		if i == 0:
			compte2=compte
			str_compte2=str_compte
			if compte == 1 and compte <= intervalles_programmes[i]:
				message                         = "*** NOUVEAU CLIENT ***"
				avis_de_suivi                   = FormaterSuivisHTML(rapport,programme_str,entrainement,compte,compte2,str_compte,str_compte2,message,programme)
				entrainement_du_jour.append(avis_de_suivi)
			elif compte >= 6 and compte % 2 == 0 and compte <= intervalles_programmes[i] or compte>10 and compte <= intervalles_programmes[i]:
				message                         = "*** SUIVI " + str(compte) + "e SÉANCE ***"
				avis_de_suivi                   = FormaterSuivisHTML(rapport,programme_str,entrainement,compte,compte2,str_compte,str_compte2,message,programme)
				entrainement_du_jour.append(avis_de_suivi)
			elif compte <= intervalles_programmes[i]:
				message                         = ""
				avis_de_suivi                   = FormaterSuivisHTML(rapport,programme_str,entrainement,compte,compte2,str_compte,str_compte2,message,programme)
				entrainement_du_jour.append(avis_de_suivi)
		elif i > 0:
			if compte > intervalles_programmes[i-1] and compte <= intervalles_programmes[i]:
				compte2 =compte-intervalles_programmes[i-1]
				str_compte2=rapport.stringCompte(compte2)
				if compte2 == 1 and compte2 <= intervalles_programmes[i]:
					message                         = "*** NOUVEAU PROGRAMME ***"
					avis_de_suivi                   = FormaterSuivisHTML(rapport,programme_str,entrainement,compte,compte2,str_compte,str_compte2,message,programme)
					entrainement_du_jour.append(avis_de_suivi)
				elif int(str_compte2) >= 6 and int(str_compte2) % 2 == 0 and int(str_compte2) <= intervalles_programmes[i] or compte2>10 and compte2 <= intervalles_programmes[i]:
					message                         = "*** SUIVI " + str(str_compte2) + "e SÉANCE ***"
					avis_de_suivi                   = FormaterSuivisHTML(rapport,programme_str,entrainement,compte,compte2,str_compte,str_compte2,message,programme)
					entrainement_du_jour.append(avis_de_suivi)
				elif compte2 <= intervalles_programmes[i]:
					message                         = ""
					avis_de_suivi                   = FormaterSuivisHTML(rapport,programme_str,entrainement,compte,compte2,str_compte,str_compte2,message,programme)
					entrainement_du_jour.append(avis_de_suivi)
	return entrainement_du_jour


# Formater un suivi patient en HTML
def FormaterSuivisHTML(rapport,programme_str,entrainement,compte,compte2,str_compte,str_compte2,message,programme):
	avis_de_suivi                   = []
	heure                           = entrainement.getHeureEntrainement()
	patient_entete                  = str(entrainement.getPatientEntrainement().getPrenom().upper()) + " " + str(entrainement.getPatientEntrainement().getNom().upper())+" "
	patient_corps                   = str(entrainement.getPatientEntrainement().getGenrePatient()) + ", " + str(entrainement.getPatientEntrainement().getAgePatient()) + " ans, " + str(entrainement.getPatientEntrainement().getCasquePatient()) + " (#" + str(entrainement.getPatientEntrainement().getIdentifiant()) + ")"
	entraineur                      = " " + str(entrainement.getEntraineurEntrainement().getPrenom()) + " " + str(entrainement.getEntraineurEntrainement().getNom()) 
	numero_seance                   = str(str_compte2) + " / " + str(programme.getNombreSeancesProgramme())
	programme_str                   = "Protocole No." + str(programme.getPrioriteCliniqueProgramme()) + " - " + str(programme.getProtocoleProgramme().getNomProtocole()) + " (#" + str(programme.getIdentifiantProgramme()) + ")"
	entrainement_id                 = entrainement.getIdentifiantEntrainement()
	cumulatif_seances               = str(compte) + " / " + str(rapport.getEntRequisRapport())
	
	descriptif                      = (patient_entete,heure,patient_corps,entraineur,numero_seance,programme_str,entrainement_id,cumulatif_seances,message)
	avis_de_suivi.append(descriptif)
	return avis_de_suivi





#############################################################################################
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#
#-*                                         SCRIPT                                        *-# 
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#
#############################################################################################


rafraichirListes()


if __name__ == '__main__':
    app.run(debug=True)




#############################################################################################
#                                                                                           #
#                                            FIN                                            # 
#                                                                                           #
#############################################################################################
